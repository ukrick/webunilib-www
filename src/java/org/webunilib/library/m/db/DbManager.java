package org.webunilib.library.m.db;

/**
 * Copyright © 2009 Jet Thoughts, Ltd. All rights reserved.
 *
 * @author Kirillov A. A.
 */
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class DbManager {

    private static DbManager instance = new DbManager();
    private Properties propertiesMap = new Properties();

    private DbManager() {
        initialize();
    }

    public static DbManager getInstance() {
        return instance;
    }

    public String getProperty(String name) {
        String props = propertiesMap.getProperty(name);
        if (props == null) {
            return new String();
        }
        return props;
    }

    private void initialize() {
        String propfile = "db.properties";
        try {
            propertiesMap = getPropertiesFromResource(propfile);
        } catch (IOException e) {
            System.out.println("Произошла ошибка при загрузке файла свойств базы данных (" +
                    propfile + "). Главная причина:  " +
                    e.toString());
        }
    }

    private Properties getPropertiesFromResource(String resource) throws IOException {
        Properties props = new Properties();
        String propfile = resource;
        InputStream in = getResourceAsStream(propfile);
        props.load(in);
        in.close();
        return props;
    }

    private InputStream getResourceAsStream(String name) throws IOException {
        InputStream in =
                this.getClass().getClassLoader().getResourceAsStream(name);
        if (in == null) {
            in = ClassLoader.getSystemResourceAsStream(name);
        }
        if (in == null) {
            throw new IOException("Невозможно найти ресурс " + name);
        }
        return in;
    }
}
