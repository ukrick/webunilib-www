package org.webunilib.library.m.db;

/**
 * Copyright © 2009 Jet Thoughts, Ltd. All rights reserved.
 *
 * @author Kirillov A. A.
 */
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.webunilib.library.m.data.InventoryInfo;
import org.webunilib.library.m.data.Records;

public class DbAccess {

    protected static final String NEWS_SEARCH_PROCEDURE =
            "SELECT * FROM NEWS_SEARCH (?)";
    protected static final String NEWS_BOOKS_PROCEDURE =
            "SELECT * FROM NEWS_BOOKS (?)";
    protected static final String WEB_SEARCH_PROCEDURE =
            "SELECT * FROM WEB_SEARCH (?)";
    protected static final String ALL_DESCRIPTION_PROCEDURE =
            "SELECT * FROM ALL_DESCRIPTION (?)";

    public DbAccess() {
    }

    protected synchronized Connection getConnection() throws Exception {
        String driverClassName = DbManager.getInstance().getProperty("driver");
        String url = DbManager.getInstance().getProperty("url");
        String password = DbManager.getInstance().getProperty("password");
        String user = DbManager.getInstance().getProperty("user");
        Class.forName(driverClassName);
        Connection con = DriverManager.getConnection(url, user, password);
        return con;
    }

    public InventoryInfo[] getNewRecords(Integer bookId) throws Exception {
        InventoryInfo[] books = null;
        Connection con = null;
        try {
            con = getConnection();
            PreparedStatement prepStmt =
                    con.prepareStatement(NEWS_SEARCH_PROCEDURE);
            prepStmt.setInt(1, bookId.intValue());
            ResultSet rs = prepStmt.executeQuery();
            List items = new java.util.ArrayList();
            while (rs.next()) {
                InventoryInfo item = new InventoryInfo();
                item.setRECNO_2(rs.getInt("RECNO_2"));
                item.setCARD_AUTHOR(rs.getString("CARD_AUTHOR"));
                item.setBASIC_DESCRIPTION(rs.getString("BASIC_DESCRIPTION"));
                item.setNOTE(rs.getString("NOTE"));
                item.setBDATE(rs.getString("BDATE"));
                items.add(item);
                books = new InventoryInfo[items.size()];
                books = (InventoryInfo[]) items.toArray(books);
            }
            prepStmt.close();
            //            con.commit(); // Calling commit() in auto-commit mode is not allowed.
            con.close();
        } catch (Exception ex) {
            try {
                if (con != null && !con.isClosed()) {
                    con.close();
                }
                throw new Exception("Ошибка выполнения запроса : "
                        + ex.getMessage());
            } catch (SQLException sqlex) {
                throw new Exception("Невозможно закрыть подключение : "
                        + sqlex.getMessage());
            }
        }
        return books;
    }

    public InventoryInfo[] getNewBooks(Integer bookId) throws Exception {
        InventoryInfo[] books = null;
        Connection con = null;
        try {
            con = getConnection();
            PreparedStatement prepStmt =
                    con.prepareStatement(NEWS_BOOKS_PROCEDURE);
            prepStmt.setInt(1, bookId.intValue());
            ResultSet rs = prepStmt.executeQuery();
            List items = new java.util.ArrayList();
            while (rs.next()) {
                InventoryInfo item = new InventoryInfo();
                item.setRECNO_2(rs.getInt("RECNO_2"));
                item.setCARD_AUTHOR(rs.getString("CARD_AUTHOR"));
                item.setHEADING(rs.getString("HEADING"));
                item.setHEADING_E(rs.getString("HEADING_E"));
                item.setPUB_INF(rs.getString("PUB_INF"));
                item.setPUB_LOCATION(rs.getString("PUB_LOCATION"));
                item.setPUB_NAME(rs.getString("PUB_NAME"));
                item.setPUB_DATE(rs.getString("PUB_DATE"));
                item.setBASIC_DESCRIPTION(rs.getString("BASIC_DESCRIPTION"));
                item.setNOTE(rs.getString("NOTE"));
                items.add(item);
                books = new InventoryInfo[items.size()];
                books = (InventoryInfo[]) items.toArray(books);
            }
            prepStmt.close();
            //            con.commit(); // Calling commit() in auto-commit mode is not allowed.
            con.close();
        } catch (Exception ex) {
            try {
                if (con != null && !con.isClosed()) {
                    con.close();
                }
                throw new Exception("Ошибка выполнения запроса : "
                        + ex.getMessage());
            } catch (SQLException sqlex) {
                throw new Exception("Невозможно закрыть подключение : "
                        + sqlex.getMessage());
            }
        }
        return books;
    }

    public InventoryInfo[] getAllItems(Integer bookId) throws Exception {
        InventoryInfo[] books = null;
        Connection con = null;
        try {
            con = getConnection();
            PreparedStatement prepStmt =
                    con.prepareStatement(WEB_SEARCH_PROCEDURE);
            prepStmt.setInt(1, bookId.intValue());
            ResultSet rs = prepStmt.executeQuery();
            List items = new java.util.ArrayList();
            while (rs.next()) {
                InventoryInfo item = new InventoryInfo();
                item.setRECNO_2(rs.getInt("RECNO_2"));
                item.setROOTID(rs.getInt("ROOTID"));
                item.setPRESS_MARK(rs.getString("PRESS_MARK"));
                item.setKEEP_CODE(rs.getString("KEEP_CODE"));
                item.setAUTHR_SIGN(rs.getString("AUTHR_SIGN"));
                item.setCARD_AUTHOR(rs.getString("CARD_AUTHOR"));
                item.setBASIC_DESCRIPTION(rs.getString("BASIC_DESCRIPTION"));
                item.setFILE_NAME(rs.getInt("FILE_NAME"));
                item.setELECTR_RES(rs.getString("ELECTR_RES"));
                item.setREPO_RES(rs.getString("REPO_RES"));
                items.add(item);
                books = new InventoryInfo[items.size()];
                books = (InventoryInfo[]) items.toArray(books);
            }
            prepStmt.close();
            //            con.commit(); // Calling commit() in auto-commit mode is not allowed.
            con.close();
        } catch (Exception ex) {
            try {
                if (con != null && !con.isClosed()) {
                    con.close();
                }
                throw new Exception("Ошибка выполнения запроса : "
                        + ex.getMessage());
            } catch (SQLException sqlex) {
                throw new Exception("Невозможно закрыть подключение : "
                        + sqlex.getMessage());
            }
        }
        return books;
    }

    public InventoryInfo[] getCard(Integer inventoryBookId) throws Exception {
        InventoryInfo[] cardbook = null;
        Connection con = null;
        try {
            con = getConnection();
            PreparedStatement prepStmt =
                    con.prepareStatement(WEB_SEARCH_PROCEDURE);
            prepStmt.setInt(1, inventoryBookId.intValue());
            ResultSet rs = prepStmt.executeQuery();
            List items = new java.util.ArrayList();
            while (rs.next()) {
                InventoryInfo item = new InventoryInfo();
                item.setRECNO_2(rs.getInt("RECNO_2"));
                item.setROOTID(rs.getInt("ROOTID"));
                item.setPRESS_MARK(rs.getString("PRESS_MARK"));
                item.setKEEP_CODE(rs.getString("KEEP_CODE"));
                item.setAUTHR_SIGN(rs.getString("AUTHR_SIGN"));
                item.setCARD_AUTHOR(rs.getString("CARD_AUTHOR"));
                item.setBASIC_DESCRIPTION(rs.getString("BASIC_DESCRIPTION"));
                item.setFILE_NAME(rs.getInt("FILE_NAME"));
                item.setELECTR_RES(rs.getString("ELECTR_RES"));
                item.setREPO_RES(rs.getString("REPO_RES"));
                items.add(item);
                cardbook = new InventoryInfo[items.size()];
                cardbook = (InventoryInfo[]) items.toArray(cardbook);
            }
            prepStmt.close();
            //            con.commit(); // Calling commit() in auto-commit mode is not allowed.
            con.close();
        } catch (Exception ex) {
            try {
                if (con != null && !con.isClosed()) {
                    con.close();
                }
                throw new Exception("Ошибка выполнения запроса : "
                        + ex.getMessage());
            } catch (SQLException sqlex) {
                throw new Exception("Невозможно закрыть подключение : "
                        + sqlex.getMessage());
            }
        }
        return cardbook;
    }

    public InventoryInfo[] getDescription(Integer detailsBookId) throws Exception {
        InventoryInfo[] description = null;
        Connection con = null;
        try {
            con = getConnection();
            PreparedStatement prepStmt =
                    con.prepareStatement(ALL_DESCRIPTION_PROCEDURE);
            prepStmt.setInt(1, detailsBookId.intValue());
            ResultSet rs = prepStmt.executeQuery();
            List items = new java.util.ArrayList();
            while (rs.next()) {
                InventoryInfo item = new InventoryInfo();
                item.setROOTID(rs.getInt("ROOTID"));
                item.setISBN(rs.getString("ISBN"));
                item.setISSN(rs.getString("ISSN"));
                item.setUDDK(rs.getString("UDDK"));
                item.setOTHK(rs.getString("OTHK"));
                item.setAUTHOR(rs.getString("AUTHOR"));
                item.setAUTHORS(rs.getString("AUTHORS"));
                item.setHEADING(rs.getString("HEADING"));
                item.setPUB_INF(rs.getString("PUB_INF"));
                item.setPUB_LOCATION(rs.getString("PUB_LOCATION"));
                item.setPUB_NAME(rs.getString("PUB_NAME"));
                item.setPUB_DATE(rs.getString("PUB_DATE"));
                item.setVOLUMES(rs.getString("VOLUMES"));
                item.setSERIES(rs.getString("SERIES"));
                item.setSUBJ_CATEGORY(rs.getString("SUBJ_CATEGORY"));
                item.setNOTE(rs.getString("NOTE"));
                item.setFILE_NAME(rs.getInt("FILE_NAME"));
                item.setELECTR_RES(rs.getString("ELECTR_RES"));
                item.setREPO_RES(rs.getString("REPO_RES"));
                items.add(item);
                description = new InventoryInfo[items.size()];
                description = (InventoryInfo[]) items.toArray(description);
            }
            prepStmt.close();
            //            con.commit(); // Calling commit() in auto-commit mode is not allowed.
            con.close();
        } catch (Exception ex) {
            try {
                if (con != null && !con.isClosed()) {
                    con.close();
                }
                throw new Exception("Ошибка выполнения запроса : "
                        + ex.getMessage());
            } catch (SQLException sqlex) {
                throw new Exception("Невозможно закрыть подключение : "
                        + sqlex.getMessage());
            }
        }
        return description;
    }

    public Records[] getSearchRecordsBySubject(String searchSubject,
            String filteredResource,
            String filteredDocument,
            String filteredDoctype,
            String filteredDocform,
            String filteredCategories,
            String filteredDateFrom,
            String filteredDateTo) throws Exception {
        Records[] records = null;
        Connection con = null;
        try {
            String selectStatement =
                    "SELECT DISTINCT DESCRIP_2.RECNO_2 FROM DESCRIP_2, SUBJ_CATEGORY_34 WHERE ((DESCRIP_2.RECNO_2 = SUBJ_CATEGORY_34.RECNO2) AND (DESCRIP_2.REF_SUBJ_CATEGORY_34 = SUBJ_CATEGORY_34.RECNO_34) AND (SUBJ_CATEGORY_34.START_DATA CONTAINING ?) AND ((DESCRIP_2.STATUS LIKE ?) AND (DESCRIP_2.STATUS LIKE ?) AND (DESCRIP_2.STATUS LIKE ?) AND (DESCRIP_2.STATUS LIKE ?) AND (DESCRIP_2.STATUS LIKE ?) AND (DESCRIP_2.BDATE >= '" + filteredDateFrom + "') AND (DESCRIP_2.BDATE <= '" + filteredDateTo + "')))";
            con = getConnection();
            PreparedStatement prepStmt = con.prepareStatement(selectStatement);
            prepStmt.setString(1, searchSubject);
            prepStmt.setString(2, filteredResource);
            prepStmt.setString(3, filteredDocument);
            prepStmt.setString(4, filteredDoctype);
            prepStmt.setString(5, filteredDocform);
            prepStmt.setString(6, filteredCategories);
            ResultSet rs = prepStmt.executeQuery();
            List items = new java.util.ArrayList();
            while (rs.next()) {
                Records item = new Records();
                item.setRecords(rs.getInt("RECNO_2"));
                items.add(item);
                records = new Records[items.size()];
                records = (Records[]) items.toArray(records);
            }
            prepStmt.close();
            //            con.commit(); // Calling commit() in auto-commit mode is not allowed.
            con.close();
        } catch (Exception ex) {
            try {
                if (con != null && !con.isClosed()) {
                    con.close();
                }
                throw new Exception("Ошибка получения строки для поиска : "
                        + ex.getMessage());
            } catch (SQLException sqlex) {
                throw new Exception("Невозможно закрыть подключение : "
                        + sqlex.getMessage());
            }
        }
        return records;
    }

    public Records[] getSearchRecordsByKeyword(String searchKeyword,
            String filteredResource,
            String filteredDocument,
            String filteredDoctype,
            String filteredDocform,
            String filteredCategories,
            String filteredDateFrom,
            String filteredDateTo) throws Exception {
        Records[] records = null;
        Connection con = null;
        try {
            String selectStatement =
                    "SELECT DISTINCT DESCRIP_2.RECNO_2 FROM DESCRIP_2, KEYWORDS_14 WHERE ((DESCRIP_2.RECNO_2 = KEYWORDS_14.RECNO2) AND (DESCRIP_2.REF_KEYWORDS_14 = KEYWORDS_14.RECNO_14) AND (KEYWORDS_14.KEYWORD CONTAINING ?) AND ((DESCRIP_2.STATUS LIKE ?) AND (DESCRIP_2.STATUS LIKE ?) AND (DESCRIP_2.STATUS LIKE ?) AND (DESCRIP_2.STATUS LIKE ?) AND (DESCRIP_2.STATUS LIKE ?) AND (DESCRIP_2.BDATE >= '" + filteredDateFrom + "') AND (DESCRIP_2.BDATE <= '" + filteredDateTo + "')))";
            con = getConnection();
            PreparedStatement prepStmt = con.prepareStatement(selectStatement);
            prepStmt.setString(1, searchKeyword);
            prepStmt.setString(2, filteredResource);
            prepStmt.setString(3, filteredDocument);
            prepStmt.setString(4, filteredDoctype);
            prepStmt.setString(5, filteredDocform);
            prepStmt.setString(6, filteredCategories);
            ResultSet rs = prepStmt.executeQuery();
            List items = new java.util.ArrayList();
            while (rs.next()) {
                Records item = new Records();
                item.setRecords(rs.getInt("RECNO_2"));
                items.add(item);
                records = new Records[items.size()];
                records = (Records[]) items.toArray(records);
            }
            prepStmt.close();
            //            con.commit(); // Calling commit() in auto-commit mode is not allowed.
            con.close();
        } catch (Exception ex) {
            try {
                if (con != null && !con.isClosed()) {
                    con.close();
                }
                throw new Exception("Ошибка получения строки для поиска : "
                        + ex.getMessage());
            } catch (SQLException sqlex) {
                throw new Exception("Невозможно закрыть подключение : "
                        + sqlex.getMessage());
            }
        }
        return records;
    }

    public Records[] getSearchRecordsByTitle(String searchTitle,
            String filteredResource,
            String filteredDocument,
            String filteredDoctype,
            String filteredDocform,
            String filteredCategories,
            String filteredDateFrom,
            String filteredDateTo) throws Exception {
        Records[] records = null;
        Connection con = null;
        try {
            String selectStatement =
                    "SELECT DISTINCT DESCRIP_2.RECNO_2 FROM DESCRIP_2, HEADINGS_18 WHERE ((DESCRIP_2.RECNO_2 = HEADINGS_18.RECNO2) AND (DESCRIP_2.REF_HEADINGS_18 = HEADINGS_18.RECNO_18) AND (HEADINGS_18.START_DATA CONTAINING ?) AND ((DESCRIP_2.STATUS LIKE ?) AND (DESCRIP_2.STATUS LIKE ?) AND (DESCRIP_2.STATUS LIKE ?) AND (DESCRIP_2.STATUS LIKE ?) AND (DESCRIP_2.STATUS LIKE ?) AND (DESCRIP_2.BDATE >= '" + filteredDateFrom + "') AND (DESCRIP_2.BDATE <= '" + filteredDateTo + "')))";
            con = getConnection();
            PreparedStatement prepStmt = con.prepareStatement(selectStatement);
            prepStmt.setString(1, searchTitle);
            prepStmt.setString(2, filteredResource);
            prepStmt.setString(3, filteredDocument);
            prepStmt.setString(4, filteredDoctype);
            prepStmt.setString(5, filteredDocform);
            prepStmt.setString(6, filteredCategories);
            ResultSet rs = prepStmt.executeQuery();
            List items = new java.util.ArrayList();
            while (rs.next()) {
                Records item = new Records();
                item.setRecords(rs.getInt("RECNO_2"));
                items.add(item);
                records = new Records[items.size()];
                records = (Records[]) items.toArray(records);
            }
            prepStmt.close();
            //            con.commit(); // Calling commit() in auto-commit mode is not allowed.
            con.close();
        } catch (Exception ex) {
            try {
                if (con != null && !con.isClosed()) {
                    con.close();
                }
                throw new Exception("Ошибка получения строки для поиска : "
                        + ex.getMessage());
            } catch (SQLException sqlex) {
                throw new Exception("Невозможно закрыть подключение : "
                        + sqlex.getMessage());
            }
        }
        return records;
    }

    public Records[] getSearchRecordsByAuthor(String searchAuthor,
            String filteredResource,
            String filteredDocument,
            String filteredDoctype,
            String filteredDocform,
            String filteredCategories,
            String filteredDateFrom,
            String filteredDateTo) throws Exception {
        Records[] records = null;
        Connection con = null;
        try {
            String selectStatement =
                    "SELECT DISTINCT DESCRIP_2.RECNO_2 FROM DESCRIP_2, AUTHORS_19 WHERE ((DESCRIP_2.RECNO_2 = AUTHORS_19.RECNO2) AND ((DESCRIP_2.REF_0_AUTHORS_19 = AUTHORS_19.RECNO_19) OR (DESCRIP_2.REF_1_AUTHORS_19 = AUTHORS_19.RECNO_19) OR (DESCRIP_2.REF_2_AUTHORS_19 = AUTHORS_19.RECNO_19)) AND (AUTHORS_19.START_DATA CONTAINING ?) AND ((DESCRIP_2.STATUS LIKE ?) AND (DESCRIP_2.STATUS LIKE ?) AND (DESCRIP_2.STATUS LIKE ?) AND (DESCRIP_2.STATUS LIKE ?) AND (DESCRIP_2.STATUS LIKE ?) AND (DESCRIP_2.BDATE >= '" + filteredDateFrom + "') AND (DESCRIP_2.BDATE <= '" + filteredDateTo + "')))";
            con = getConnection();
            PreparedStatement prepStmt = con.prepareStatement(selectStatement);
            prepStmt.setString(1, searchAuthor);
            prepStmt.setString(2, filteredResource);
            prepStmt.setString(3, filteredDocument);
            prepStmt.setString(4, filteredDoctype);
            prepStmt.setString(5, filteredDocform);
            prepStmt.setString(6, filteredCategories);
            ResultSet rs = prepStmt.executeQuery();
            List items = new java.util.ArrayList();
            while (rs.next()) {
                Records item = new Records();
                item.setRecords(rs.getInt("RECNO_2"));
                items.add(item);
                records = new Records[items.size()];
                records = (Records[]) items.toArray(records);
            }
            prepStmt.close();
            //            con.commit(); // Calling commit() in auto-commit mode is not allowed.
            con.close();
        } catch (Exception ex) {
            try {
                if (con != null && !con.isClosed()) {
                    con.close();
                }
                throw new Exception("Ошибка получения строки для поиска : "
                        + ex.getMessage());
            } catch (SQLException sqlex) {
                throw new Exception("Невозможно закрыть подключение : "
                        + sqlex.getMessage());
            }
        }
        return records;
    }

    public Records[] getSearchRecordsByPubLoc(String searchPubLoc,
            String filteredResource,
            String filteredDocument,
            String filteredDoctype,
            String filteredDocform,
            String filteredCategories,
            String filteredDateFrom,
            String filteredDateTo) throws Exception {
        Records[] records = null;
        Connection con = null;
        try {
            String selectStatement =
                    "SELECT DISTINCT DESCRIP_2.RECNO_2 FROM DESCRIP_2, PUBLIC_SPR_10 WHERE ((DESCRIP_2.RECNO_2 = PUBLIC_SPR_10.RECNO2) AND (DESCRIP_2.REF_PUBLIC_SPR_10 = PUBLIC_SPR_10.RECNO_10) AND (PUBLIC_SPR_10.PUB_LOCATION CONTAINING ?) AND ((DESCRIP_2.STATUS LIKE ?) AND (DESCRIP_2.STATUS LIKE ?) AND (DESCRIP_2.STATUS LIKE ?) AND (DESCRIP_2.STATUS LIKE ?) AND (DESCRIP_2.STATUS LIKE ?) AND (DESCRIP_2.BDATE >= '" + filteredDateFrom + "') AND (DESCRIP_2.BDATE <= '" + filteredDateTo + "')))";
            con = getConnection();
            PreparedStatement prepStmt = con.prepareStatement(selectStatement);
            prepStmt.setString(1, searchPubLoc);
            prepStmt.setString(2, filteredResource);
            prepStmt.setString(3, filteredDocument);
            prepStmt.setString(4, filteredDoctype);
            prepStmt.setString(5, filteredDocform);
            prepStmt.setString(6, filteredCategories);
            ResultSet rs = prepStmt.executeQuery();
            List items = new java.util.ArrayList();
            while (rs.next()) {
                Records item = new Records();
                item.setRecords(rs.getInt("RECNO_2"));
                items.add(item);
                records = new Records[items.size()];
                records = (Records[]) items.toArray(records);
            }
            prepStmt.close();
            //            con.commit(); // Calling commit() in auto-commit mode is not allowed.
            con.close();
        } catch (Exception ex) {
            try {
                if (con != null && !con.isClosed()) {
                    con.close();
                }
                throw new Exception("Ошибка получения строки для поиска : "
                        + ex.getMessage());
            } catch (SQLException sqlex) {
                throw new Exception("Невозможно закрыть подключение : "
                        + sqlex.getMessage());
            }
        }
        return records;
    }

    public Records[] getSearchRecordsByPubHouse(String searchPubHouse,
            String filteredResource,
            String filteredDocument,
            String filteredDoctype,
            String filteredDocform,
            String filteredCategories,
            String filteredDateFrom,
            String filteredDateTo) throws Exception {
        Records[] records = null;
        Connection con = null;
        try {
            String selectStatement =
                    "SELECT DISTINCT DESCRIP_2.RECNO_2 FROM DESCRIP_2, PUBLIC_SPR_10 WHERE ((DESCRIP_2.RECNO_2 = PUBLIC_SPR_10.RECNO2) AND (DESCRIP_2.REF_PUBLIC_SPR_10 = PUBLIC_SPR_10.RECNO_10) AND (PUBLIC_SPR_10.PUB_NAME CONTAINING ?) AND ((DESCRIP_2.STATUS LIKE ?) AND (DESCRIP_2.STATUS LIKE ?) AND (DESCRIP_2.STATUS LIKE ?) AND (DESCRIP_2.STATUS LIKE ?) AND (DESCRIP_2.STATUS LIKE ?) AND (DESCRIP_2.BDATE >= '" + filteredDateFrom + "') AND (DESCRIP_2.BDATE <= '" + filteredDateTo + "')))";
            con = getConnection();
            PreparedStatement prepStmt = con.prepareStatement(selectStatement);
            prepStmt.setString(1, searchPubHouse);
            prepStmt.setString(2, filteredResource);
            prepStmt.setString(3, filteredDocument);
            prepStmt.setString(4, filteredDoctype);
            prepStmt.setString(5, filteredDocform);
            prepStmt.setString(6, filteredCategories);
            ResultSet rs = prepStmt.executeQuery();
            List items = new java.util.ArrayList();
            while (rs.next()) {
                Records item = new Records();
                item.setRecords(rs.getInt("RECNO_2"));
                items.add(item);
                records = new Records[items.size()];
                records = (Records[]) items.toArray(records);
            }
            prepStmt.close();
            //            con.commit(); // Calling commit() in auto-commit mode is not allowed.
            con.close();
        } catch (Exception ex) {
            try {
                if (con != null && !con.isClosed()) {
                    con.close();
                }
                throw new Exception("Ошибка получения строки для поиска : "
                        + ex.getMessage());
            } catch (SQLException sqlex) {
                throw new Exception("Невозможно закрыть подключение : "
                        + sqlex.getMessage());
            }
        }
        return records;
    }

    public Records[] getSearchRecordsByPubDate(String searchPubDate,
            String filteredResource,
            String filteredDocument,
            String filteredDoctype,
            String filteredDocform,
            String filteredCategories,
            String filteredDateFrom,
            String filteredDateTo) throws Exception {
        Records[] records = null;
        Connection con = null;
        try {
            String selectStatement =
                    "SELECT DISTINCT DESCRIP_2.RECNO_2 FROM DESCRIP_2, PUBLIC_SPR_10 WHERE ((DESCRIP_2.RECNO_2 = PUBLIC_SPR_10.RECNO2) AND (DESCRIP_2.REF_PUBLIC_SPR_10 = PUBLIC_SPR_10.RECNO_10) AND (PUBLIC_SPR_10.PUB_DATE CONTAINING ?) AND ((DESCRIP_2.STATUS LIKE ?) AND (DESCRIP_2.STATUS LIKE ?) AND (DESCRIP_2.STATUS LIKE ?) AND (DESCRIP_2.STATUS LIKE ?) AND (DESCRIP_2.STATUS LIKE ?) AND (DESCRIP_2.BDATE >= '" + filteredDateFrom + "') AND (DESCRIP_2.BDATE <= '" + filteredDateTo + "')))";
            con = getConnection();
            PreparedStatement prepStmt = con.prepareStatement(selectStatement);
            prepStmt.setString(1, searchPubDate);
            prepStmt.setString(2, filteredResource);
            prepStmt.setString(3, filteredDocument);
            prepStmt.setString(4, filteredDoctype);
            prepStmt.setString(5, filteredDocform);
            prepStmt.setString(6, filteredCategories);
            ResultSet rs = prepStmt.executeQuery();
            List items = new java.util.ArrayList();
            while (rs.next()) {
                Records item = new Records();
                item.setRecords(rs.getInt("RECNO_2"));
                items.add(item);
                records = new Records[items.size()];
                records = (Records[]) items.toArray(records);
            }
            prepStmt.close();
            //            con.commit(); // Calling commit() in auto-commit mode is not allowed.
            con.close();
        } catch (Exception ex) {
            try {
                if (con != null && !con.isClosed()) {
                    con.close();
                }
                throw new Exception("Ошибка получения строки для поиска : "
                        + ex.getMessage());
            } catch (SQLException sqlex) {
                throw new Exception("Невозможно закрыть подключение : "
                        + sqlex.getMessage());
            }
        }
        return records;
    }

    public Records[] getSearchRecordsByISXN(String searchISXN,
            String filteredResource,
            String filteredDocument,
            String filteredDoctype,
            String filteredDocform,
            String filteredCategories,
            String filteredDateFrom,
            String filteredDateTo) throws Exception {
        Records[] records = null;
        Connection con = null;
        try {
            String selectStatement =
                    "SELECT DISTINCT DESCRIP_2.RECNO_2 FROM DESCRIP_2, ISBX_3 WHERE ((DESCRIP_2.RECNO_2 = ISBX_3.RECNO2) AND ((DESCRIP_2.REF_0_ISBX_3 = ISBX_3.RECNO_3) OR (DESCRIP_2.REF_1_ISBX_3 = ISBX_3.RECNO_3)) AND (ISBX_3.ISXN CONTAINING ?) AND ((DESCRIP_2.STATUS LIKE ?) AND (DESCRIP_2.STATUS LIKE ?) AND (DESCRIP_2.STATUS LIKE ?) AND (DESCRIP_2.STATUS LIKE ?) AND (DESCRIP_2.STATUS LIKE ?) AND (DESCRIP_2.BDATE >= '" + filteredDateFrom + "') AND (DESCRIP_2.BDATE <= '" + filteredDateTo + "')))";
            con = getConnection();
            PreparedStatement prepStmt = con.prepareStatement(selectStatement);
            prepStmt.setString(1, searchISXN);
            prepStmt.setString(2, filteredResource);
            prepStmt.setString(3, filteredDocument);
            prepStmt.setString(4, filteredDoctype);
            prepStmt.setString(5, filteredDocform);
            prepStmt.setString(6, filteredCategories);
            ResultSet rs = prepStmt.executeQuery();
            List items = new java.util.ArrayList();
            while (rs.next()) {
                Records item = new Records();
                item.setRecords(rs.getInt("RECNO_2"));
                items.add(item);
                records = new Records[items.size()];
                records = (Records[]) items.toArray(records);
            }
            prepStmt.close();
            //            con.commit(); // Calling commit() in auto-commit mode is not allowed.
            con.close();
        } catch (Exception ex) {
            try {
                if (con != null && !con.isClosed()) {
                    con.close();
                }
                throw new Exception("Ошибка получения строки для поиска : "
                        + ex.getMessage());
            } catch (SQLException sqlex) {
                throw new Exception("Невозможно закрыть подключение : "
                        + sqlex.getMessage());
            }
        }
        return records;
    }

    public Records[] getSearchRecordsByUDDK(String searchUDDK,
            String filteredResource,
            String filteredDocument,
            String filteredDoctype,
            String filteredDocform,
            String filteredCategories,
            String filteredDateFrom,
            String filteredDateTo) throws Exception {
        Records[] records = null;
        Connection con = null;
        try {
            String selectStatement =
                    "SELECT DISTINCT DESCRIP_2.RECNO_2 FROM DESCRIP_2, UDDK_16 WHERE ((DESCRIP_2.RECNO_2 = UDDK_16.RECNO2) AND ((DESCRIP_2.REF_5_UDDK_16 = UDDK_16.RECNO_16) OR (DESCRIP_2.REF_6_UDDK_16 = UDDK_16.RECNO_16)) AND (UDDK_16.UD_INDEX CONTAINING ?) AND ((DESCRIP_2.STATUS LIKE ?) AND (DESCRIP_2.STATUS LIKE ?) AND (DESCRIP_2.STATUS LIKE ?) AND (DESCRIP_2.STATUS LIKE ?) AND (DESCRIP_2.STATUS LIKE ?) AND (DESCRIP_2.BDATE >= '" + filteredDateFrom + "') AND (DESCRIP_2.BDATE <= '" + filteredDateTo + "')))";
            con = getConnection();
            PreparedStatement prepStmt = con.prepareStatement(selectStatement);
            prepStmt.setString(1, searchUDDK);
            prepStmt.setString(2, filteredResource);
            prepStmt.setString(3, filteredDocument);
            prepStmt.setString(4, filteredDoctype);
            prepStmt.setString(5, filteredDocform);
            prepStmt.setString(6, filteredCategories);
            ResultSet rs = prepStmt.executeQuery();
            List items = new java.util.ArrayList();
            while (rs.next()) {
                Records item = new Records();
                item.setRecords(rs.getInt("RECNO_2"));
                items.add(item);
                records = new Records[items.size()];
                records = (Records[]) items.toArray(records);
            }
            prepStmt.close();
            //            con.commit(); // Calling commit() in auto-commit mode is not allowed.
            con.close();
        } catch (Exception ex) {
            try {
                if (con != null && !con.isClosed()) {
                    con.close();
                }
                throw new Exception("Ошибка получения строки для поиска : "
                        + ex.getMessage());
            } catch (SQLException sqlex) {
                throw new Exception("Невозможно закрыть подключение : "
                        + sqlex.getMessage());
            }
        }
        return records;
    }

    public Records[] getSearchRecordsByOTHK(String searchOTHK,
            String filteredResource,
            String filteredDocument,
            String filteredDoctype,
            String filteredDocform,
            String filteredCategories,
            String filteredDateFrom,
            String filteredDateTo) throws Exception {
        Records[] records = null;
        Connection con = null;
        try {
            String selectStatement =
                    "SELECT DISTINCT DESCRIP_2.RECNO_2 FROM DESCRIP_2, OTHK_24 WHERE ((DESCRIP_2.RECNO_2 = OTHK_24.RECNO2) AND (DESCRIP_2.REF_OTHK_24 = OTHK_24.RECNO_24) AND (OTHK_24.CLASS_INDEX CONTAINING ?) AND ((DESCRIP_2.STATUS LIKE ?) AND (DESCRIP_2.STATUS LIKE ?) AND (DESCRIP_2.STATUS LIKE ?) AND (DESCRIP_2.STATUS LIKE ?) AND (DESCRIP_2.STATUS LIKE ?) AND (DESCRIP_2.BDATE >= '" + filteredDateFrom + "') AND (DESCRIP_2.BDATE <= '" + filteredDateTo + "')))";
            con = getConnection();
            PreparedStatement prepStmt = con.prepareStatement(selectStatement);
            prepStmt.setString(1, searchOTHK);
            prepStmt.setString(2, filteredResource);
            prepStmt.setString(3, filteredDocument);
            prepStmt.setString(4, filteredDoctype);
            prepStmt.setString(5, filteredDocform);
            prepStmt.setString(6, filteredCategories);
            ResultSet rs = prepStmt.executeQuery();
            List items = new java.util.ArrayList();
            while (rs.next()) {
                Records item = new Records();
                item.setRecords(rs.getInt("RECNO_2"));
                items.add(item);
                records = new Records[items.size()];
                records = (Records[]) items.toArray(records);
            }
            prepStmt.close();
            //            con.commit(); // Calling commit() in auto-commit mode is not allowed.
            con.close();
        } catch (Exception ex) {
            try {
                if (con != null && !con.isClosed()) {
                    con.close();
                }
                throw new Exception("Ошибка получения строки для поиска : "
                        + ex.getMessage());
            } catch (SQLException sqlex) {
                throw new Exception("Невозможно закрыть подключение : "
                        + sqlex.getMessage());
            }
        }
        return records;
    }

    public Records[] getSearchRecordsByInvNum(String searchInvNum,
            String filteredResource,
            String filteredDocument,
            String filteredDoctype,
            String filteredDocform,
            String filteredCategories,
            String filteredDateFrom,
            String filteredDateTo) throws Exception {
        Records[] records = null;
        Connection con = null;
        try {
            String selectStatement =
                    "SELECT DISTINCT DESCRIP_2.RECNO_2 FROM DESCRIP_2, LOCATIONS_23 WHERE ((DESCRIP_2.RECNO_2 = LOCATIONS_23.RECNO2) AND (DESCRIP_2.REF_LOCATIONS_23 = LOCATIONS_23.RECNO_23) AND (LOCATIONS_23.INVNUM CONTAINING ?) AND ((DESCRIP_2.STATUS LIKE ?) AND (DESCRIP_2.STATUS LIKE ?) AND (DESCRIP_2.STATUS LIKE ?) AND (DESCRIP_2.STATUS LIKE ?) AND (DESCRIP_2.STATUS LIKE ?) AND (DESCRIP_2.BDATE >= '" + filteredDateFrom + "') AND (DESCRIP_2.BDATE <= '" + filteredDateTo + "')))";
            con = getConnection();
            PreparedStatement prepStmt = con.prepareStatement(selectStatement);
            prepStmt.setString(1, searchInvNum);
            prepStmt.setString(2, filteredResource);
            prepStmt.setString(3, filteredDocument);
            prepStmt.setString(4, filteredDoctype);
            prepStmt.setString(5, filteredDocform);
            prepStmt.setString(6, filteredCategories);
            ResultSet rs = prepStmt.executeQuery();
            List items = new java.util.ArrayList();
            while (rs.next()) {
                Records item = new Records();
                item.setRecords(rs.getInt("RECNO_2"));
                items.add(item);
                records = new Records[items.size()];
                records = (Records[]) items.toArray(records);
            }
            prepStmt.close();
            //            con.commit(); // Calling commit() in auto-commit mode is not allowed.
            con.close();
        } catch (Exception ex) {
            try {
                if (con != null && !con.isClosed()) {
                    con.close();
                }
                throw new Exception("Ошибка получения строки для поиска : "
                        + ex.getMessage());
            } catch (SQLException sqlex) {
                throw new Exception("Невозможно закрыть подключение : "
                        + sqlex.getMessage());
            }
        }
        return records;
    }

    public Records[] getSearchRecordsByCodeStore(String searchCodeStore,
            String filteredResource,
            String filteredDocument,
            String filteredDoctype,
            String filteredDocform,
            String filteredCategories,
            String filteredDateFrom,
            String filteredDateTo) throws Exception {
        Records[] records = null;
        Connection con = null;
        try {
            String selectStatement =
                    "SELECT DISTINCT DESCRIP_2.RECNO_2 FROM DESCRIP_2, LOCATIONS_23 WHERE ((DESCRIP_2.RECNO_2 = LOCATIONS_23.RECNO2) AND (DESCRIP_2.REF_LOCATIONS_23 = LOCATIONS_23.RECNO_23) AND ((LOCATIONS_23.PRESS_MARK CONTAINING ?) OR (LOCATIONS_23.KEEP_CODE CONTAINING ?) OR (LOCATIONS_23.AUTHR_SIGN CONTAINING ?) OR (LOCATIONS_23.SIGLA CONTAINING ?)) AND ((DESCRIP_2.STATUS LIKE ?) AND (DESCRIP_2.STATUS LIKE ?) AND (DESCRIP_2.STATUS LIKE ?) AND (DESCRIP_2.STATUS LIKE ?) AND (DESCRIP_2.STATUS LIKE ?) AND (DESCRIP_2.BDATE >= '" + filteredDateFrom + "') AND (DESCRIP_2.BDATE <= '" + filteredDateTo + "')))";
            con = getConnection();
            PreparedStatement prepStmt = con.prepareStatement(selectStatement);
            prepStmt.setString(1, searchCodeStore);
            prepStmt.setString(2, searchCodeStore);
            prepStmt.setString(3, searchCodeStore);
            prepStmt.setString(4, searchCodeStore);
            prepStmt.setString(5, filteredResource);
            prepStmt.setString(6, filteredDocument);
            prepStmt.setString(7, filteredDoctype);
            prepStmt.setString(8, filteredDocform);
            prepStmt.setString(9, filteredCategories);
            ResultSet rs = prepStmt.executeQuery();
            List items = new java.util.ArrayList();
            while (rs.next()) {
                Records item = new Records();
                item.setRecords(rs.getInt("RECNO_2"));
                items.add(item);
                records = new Records[items.size()];
                records = (Records[]) items.toArray(records);
            }
            prepStmt.close();
            //            con.commit(); // Calling commit() in auto-commit mode is not allowed.
            con.close();
        } catch (Exception ex) {
            try {
                if (con != null && !con.isClosed()) {
                    con.close();
                }
                throw new Exception("Ошибка получения строки для поиска : "
                        + ex.getMessage());
            } catch (SQLException sqlex) {
                throw new Exception("Невозможно закрыть подключение : "
                        + sqlex.getMessage());
            }
        }
        return records;
    }

    public InventoryInfo[] getInventory(Integer inventoryBookId) throws Exception {
        InventoryInfo[] inventory = null;
        Connection con = null;
        try {
            String selectStatement =
                    "SELECT DISTINCT LOCATIONS_23.INVNUM, LOCATIONS_23.PRESS_MARK, LOCATIONS_23.KEEP_CODE, LOCATIONS_23.AUTHR_SIGN, LOCATIONS_23.SIGLA FROM LOCATIONS_23 WHERE ((LOCATIONS_23.RECNO2 = ?))";
            con = getConnection();
            PreparedStatement prepStmt = con.prepareStatement(selectStatement);
            prepStmt.setInt(1, inventoryBookId.intValue());
            ResultSet rs = prepStmt.executeQuery();
            List items = new java.util.ArrayList();
            while (rs.next()) {
                InventoryInfo item = new InventoryInfo();
                item.setINVNUM(new Double(rs.getDouble("INVNUM")));
                item.setPRESS_MARK(rs.getString("PRESS_MARK"));
                item.setKEEP_CODE(rs.getString("KEEP_CODE"));
                item.setAUTHR_SIGN(rs.getString("AUTHR_SIGN"));
                item.setSIGLA(rs.getString("SIGLA"));
                items.add(item);
                inventory = new InventoryInfo[items.size()];
                inventory = (InventoryInfo[]) items.toArray(inventory);
            }
            prepStmt.close();
            //            con.commit(); // Calling commit() in auto-commit mode is not allowed.
            con.close();
        } catch (Exception ex) {
            try {
                if (con != null && !con.isClosed()) {
                    con.close();
                }
                throw new Exception("Ошибка выполнения запроса : "
                        + ex.getMessage());
            } catch (SQLException sqlex) {
                throw new Exception("Невозможно закрыть подключение : "
                        + sqlex.getMessage());
            }
        }
        return inventory;
    }

    public Records[] getVolumes(Integer multivolumeBookId) throws Exception {
        Records[] records = null;
        Connection con = null;
        try {
            String selectStatement =
                    "SELECT DISTINCT ENCLOSURES_26.ENCLOSURE FROM ENCLOSURES_26 WHERE ((ENCLOSURES_26.RECNO2 = ?) AND (ENCLOSURES_26.ID = -2))";
            con = getConnection();
            PreparedStatement prepStmt = con.prepareStatement(selectStatement);
            prepStmt.setInt(1, multivolumeBookId.intValue());
            ResultSet rs = prepStmt.executeQuery();
            List items = new java.util.ArrayList();
            while (rs.next()) {
                Records item = new Records();
                item.setRecords(rs.getInt("ENCLOSURE"));
                items.add(item);
                records = new Records[items.size()];
                records = (Records[]) items.toArray(records);
            }
            prepStmt.close();
            //            con.commit(); // Calling commit() in auto-commit mode is not allowed.
            con.close();
        } catch (Exception ex) {
            try {
                if (con != null && !con.isClosed()) {
                    con.close();
                }
                throw new Exception("Ошибка выполнения запроса : "
                        + ex.getMessage());
            } catch (SQLException sqlex) {
                throw new Exception("Невозможно закрыть подключение : "
                        + sqlex.getMessage());
            }
        }
        return records;
    }

    public Integer getMultiVolumesBookId(Integer volumeBookId) throws Exception {
        Integer multivolumeBookId;
        Connection con = null;
        try {
            String selectStatement =
                    "SELECT DISTINCT ENCLOSURES_26.ENCLOSURE FROM ENCLOSURES_26 WHERE ((ENCLOSURES_26.RECNO2 = ?) AND (ENCLOSURES_26.ID = -1))";
            con = getConnection();
            PreparedStatement prepStmt = con.prepareStatement(selectStatement);
            prepStmt.setInt(1, volumeBookId.intValue());
            ResultSet rs = prepStmt.executeQuery();
            rs.next();
            multivolumeBookId = rs.getInt("ENCLOSURE");
            prepStmt.close();
            //            con.commit(); // Calling commit() in auto-commit mode is not allowed.
            con.close();
        } catch (Exception ex) {
            try {
                if (con != null && !con.isClosed()) {
                    con.close();
                }
                throw new Exception("Ошибка выполнения запроса : "
                        + ex.getMessage());
            } catch (SQLException sqlex) {
                throw new Exception("Невозможно закрыть подключение : "
                        + sqlex.getMessage());
            }
        }
        return multivolumeBookId;
    }

    public Integer getSerialEditionId(Integer volumeBookId) throws Exception {
        Integer multivolumeBookId;
        Connection con = null;
        try {
            String selectStatement
                    = "SELECT DISTINCT ENCLOSURES_26.ENCLOSURE FROM ENCLOSURES_26 WHERE ((ENCLOSURES_26.RECNO2 = ?) AND (ENCLOSURES_26.ID = 25))";
            con = getConnection();
            PreparedStatement prepStmt = con.prepareStatement(selectStatement);
            prepStmt.setInt(1, volumeBookId.intValue());
            ResultSet rs = prepStmt.executeQuery();
            rs.next();
            multivolumeBookId = rs.getInt("ENCLOSURE");
            prepStmt.close();
            //            con.commit(); // Calling commit() in auto-commit mode is not allowed.
            con.close();
        } catch (Exception ex) {
            try {
                if (con != null && !con.isClosed()) {
                    con.close();
                }
                throw new Exception("Ошибка выполнения запроса : "
                        + ex.getMessage());
            } catch (SQLException sqlex) {
                throw new Exception("Невозможно закрыть подключение : "
                        + sqlex.getMessage());
            }
        }
        return multivolumeBookId;
    }

    public Records[] getArticles(Integer multivolumeBookId) throws Exception {
        Records[] records = null;
        Connection con = null;
        try {
            String selectStatement
                    = "SELECT DISTINCT ENCLOSURES_26.ENCLOSURE FROM ENCLOSURES_26 WHERE ((ENCLOSURES_26.RECNO2 = ?) AND (ENCLOSURES_26.ID = 26))";
            con = getConnection();
            PreparedStatement prepStmt = con.prepareStatement(selectStatement);
            prepStmt.setInt(1, multivolumeBookId.intValue());
            ResultSet rs = prepStmt.executeQuery();
            List items = new java.util.ArrayList();
            while (rs.next()) {
                Records item = new Records();
                item.setRecords(rs.getInt("ENCLOSURE"));
                items.add(item);
                records = new Records[items.size()];
                records = (Records[]) items.toArray(records);
            }
            prepStmt.close();
            //            con.commit(); // Calling commit() in auto-commit mode is not allowed.
            con.close();
        } catch (Exception ex) {
            try {
                if (con != null && !con.isClosed()) {
                    con.close();
                }
                throw new Exception("Ошибка выполнения запроса : "
                        + ex.getMessage());
            } catch (SQLException sqlex) {
                throw new Exception("Невозможно закрыть подключение : "
                        + sqlex.getMessage());
            }
        }
        return records;
    }

    public Records[] getBooksRecords(String searchFrom, String searchTo) throws Exception {
        Records[] records = null;
        Connection con = null;
        try {
            String selectStatement =
                    "SELECT DISTINCT DESCRIP_2.RECNO_2 FROM DESCRIP_2 WHERE ((DESCRIP_2.ROOTID BETWEEN 1 AND 2) AND (DESCRIP_2.BDATE >= '" + searchFrom + "') AND (DESCRIP_2.BDATE <= '" + searchTo + "'))";
            con = getConnection();
            PreparedStatement prepStmt = con.prepareStatement(selectStatement);
            ResultSet rs = prepStmt.executeQuery();
            List items = new java.util.ArrayList();
            while (rs.next()) {
                Records item = new Records();
                item.setRecords(rs.getInt("RECNO_2"));
                items.add(item);
                records = new Records[items.size()];
                records = (Records[]) items.toArray(records);
            }
            prepStmt.close();
            //            con.commit(); // Calling commit() in auto-commit mode is not allowed.
            con.close();
        } catch (Exception ex) {
            try {
                if (con != null && !con.isClosed()) {
                    con.close();
                }
                throw new Exception("Ошибка выполнения запроса : "
                        + ex.getMessage());
            } catch (SQLException sqlex) {
                throw new Exception("Невозможно закрыть подключение : "
                        + sqlex.getMessage());
            }
        }
        return records;
    }

    public Records[] getBooksRecords() throws Exception {
        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.MONTH, -1);
        SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy hh:mm");
        String searchFrom = formatter.format(calendar.getTime());
        String searchTo = formatter.format(date);
        Records[] records = null;
        Connection con = null;
        try {
            String selectStatement =
                    "SELECT DISTINCT DESCRIP_2.RECNO_2 FROM DESCRIP_2 WHERE ((DESCRIP_2.ROOTID BETWEEN 1 AND 2) AND (DESCRIP_2.BDATE >= '" + searchFrom + "') AND (DESCRIP_2.BDATE <= '" + searchTo + "'))";
            con = getConnection();
            PreparedStatement prepStmt = con.prepareStatement(selectStatement);
            ResultSet rs = prepStmt.executeQuery();
            List items = new java.util.ArrayList();
            while (rs.next()) {
                Records item = new Records();
                item.setRecords(rs.getInt("RECNO_2"));
                items.add(item);
                records = new Records[items.size()];
                records = (Records[]) items.toArray(records);
            }
            prepStmt.close();
            //            con.commit(); // Calling commit() in auto-commit mode is not allowed.
            con.close();
        } catch (Exception ex) {
            try {
                if (con != null && !con.isClosed()) {
                    con.close();
                }
                throw new Exception("Ошибка выполнения запроса : "
                        + ex.getMessage());
            } catch (SQLException sqlex) {
                throw new Exception("Невозможно закрыть подключение : "
                        + sqlex.getMessage());
            }
        }
        return records;
    }
}
