package org.webunilib.library.m.data;

/**
 * Copyright © 2009 Jet Thoughts, Ltd. All rights reserved.
 *
 * @author Kirillov A. A.
 */
public class Records implements java.io.Serializable {

    public Records() {
    }
    private transient Object records;

    public Object getRecords() {
        return records;
    }

    public void setRecords(Object records) {
        this.records = records;
    }
}
