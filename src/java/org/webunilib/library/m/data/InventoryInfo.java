package org.webunilib.library.m.data;

import java.io.IOException;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

/**
 * Copyright © 2009 Jet Thoughts, Ltd. All rights reserved.
 *
 * @author Kirillov A. A.
 */
@ManagedBean
@RequestScoped
public class InventoryInfo implements Serializable {

    protected String ISBN;
    protected String ISSN;
    protected Double INVNUM;
    protected String stringINVNUM;
    protected String PRESS_MARK;
    protected String KEEP_CODE;
    protected String AUTHR_SIGN;
    protected String SIGLA;
    protected String UDDK;
    protected String OTHK;
    protected String AUTHOR;
    protected String AUTHORS;
    protected String HEADING;
    protected String HEADING_E;
    protected String PUB_INF;
    protected String PUB_LOCATION;
    protected String PUB_NAME;
    protected String PUB_DATE;
    protected String VOLUMES;
    protected String SERIES;
    protected String SUBJ_CATEGORY;
    protected String NOTE;
    protected Integer FILE_NAME;
    protected String ELECTR_RES;
    protected String REPO_RES;    
    protected String CARD_AUTHOR;
    protected String BASIC_DESCRIPTION;
    protected String STATUS;
    protected Integer ROOTID;
    protected Integer RECNO_2;
    protected Integer ENCLOSURE;
    protected String BDATE;
    protected String ftpStyle;
    protected String repoStyle;
    protected String imagePDF;
    protected String imageFTP;
    protected String imageURL;
    protected String resource;
    protected String location;
    private Locale locale;
    protected final String baseName = "org.webunilib.library.bundle";
    protected transient ResourceBundle resources;

    public InventoryInfo() {
        init();
    }

    private void init() {
    }

    private void addMessage(String message) {
        FacesContext context = FacesContext.getCurrentInstance();
        FacesMessage msg = new FacesMessage(message);
        context.addMessage(null, msg);
    }

    public String getISBN() {
        return ISBN;
    }

    public void setISBN(String ISBN) {
        this.ISBN = ISBN;
    }

    public String getISSN() {
        return ISSN;
    }

    public void setISSN(String ISSN) {
        this.ISSN = ISSN;
    }

    public Double getINVNUM() {
        return INVNUM;
    }

    public void setINVNUM(Double INVNUM) {
        this.INVNUM = INVNUM;
    }

    public String getStringINVNUM() {
        stringINVNUM = Double.toString(INVNUM - Math.pow(10, 10));
        if (INVNUM == 0.0) {
            return stringINVNUM.substring(0, 0);
        } else if (INVNUM < 10000099999.0) {
            return stringINVNUM.substring(0, 5);
        } else {
            return stringINVNUM.substring(0, 6);
        }
    }

    public void setStringINVNUM(String stringINVNUM) {
        this.stringINVNUM = stringINVNUM;
    }

    public String getPRESS_MARK() {
        return PRESS_MARK;
    }

    public void setPRESS_MARK(String PRESS_MARK) {
        this.PRESS_MARK = PRESS_MARK;
    }

    public String getKEEP_CODE() {
        return KEEP_CODE;
    }

    public void setKEEP_CODE(String KEEP_CODE) {
        this.KEEP_CODE = KEEP_CODE;
    }

    public String getAUTHR_SIGN() {
        return AUTHR_SIGN;
    }

    public void setAUTHR_SIGN(String AUTHR_SIGN) {
        this.AUTHR_SIGN = AUTHR_SIGN;
    }

    public String getSIGLA() {
        return SIGLA;
    }

    public void setSIGLA(String SIGLA) {
        this.SIGLA = SIGLA;
    }

    public String getUDDK() {
        return UDDK;
    }

    public void setUDDK(String UDDK) {
        this.UDDK = UDDK;
    }

    public String getOTHK() {
        return OTHK;
    }

    public void setOTHK(String OTHK) {
        this.OTHK = OTHK;
    }

    public String getAUTHOR() {
        return AUTHOR;
    }

    public void setAUTHOR(String AUTHOR) {
        this.AUTHOR = AUTHOR;
    }

    public String getAUTHORS() {
        return AUTHORS;
    }

    public void setAUTHORS(String AUTHORS) {
        this.AUTHORS = AUTHORS;
    }

    public String getHEADING() {
        return HEADING;
    }

    public void setHEADING(String HEADING) {
        this.HEADING = HEADING;
    }

    public String getHEADING_E() {
        return HEADING_E;
    }

    public void setHEADING_E(String HEADING_E) {
        this.HEADING_E = HEADING_E;
    }

    public String getPUB_INF() {
        return PUB_INF;
    }

    public void setPUB_INF(String PUB_INF) {
        this.PUB_INF = PUB_INF;
    }

    public String getPUB_LOCATION() {
        return PUB_LOCATION;
    }

    public void setPUB_LOCATION(String PUB_LOCATION) {
        this.PUB_LOCATION = PUB_LOCATION;
    }

    public String getPUB_NAME() {
        return PUB_NAME;
    }

    public void setPUB_NAME(String PUB_NAME) {
        this.PUB_NAME = PUB_NAME;
    }

    public String getPUB_DATE() {
        return PUB_DATE;
    }

    public void setPUB_DATE(String PUB_DATE) {
        this.PUB_DATE = PUB_DATE;
    }

    public String getVOLUMES() {
        return VOLUMES;
    }

    public void setVOLUMES(String VOLUMES) {
        this.VOLUMES = VOLUMES;
    }

    public String getSERIES() {
        return SERIES;
    }

    public void setSERIES(String SERIES) {
        this.SERIES = SERIES;
    }

    public String getSUBJ_CATEGORY() {
        return SUBJ_CATEGORY;
    }

    public void setSUBJ_CATEGORY(String SUBJ_CATEGORY) {
        this.SUBJ_CATEGORY = SUBJ_CATEGORY;
    }

    public String getNOTE() {
        return NOTE;
    }

    public void setNOTE(String NOTE) {
        this.NOTE = NOTE;
    }

    public Integer getFILE_NAME() {
        return FILE_NAME;
    }

    public void setFILE_NAME(Integer FILE_NAME) {
        this.FILE_NAME = FILE_NAME;
    }
    
    public String getELECTR_RES() {
        return ELECTR_RES;
    }

    public void setELECTR_RES(String ELECTR_RES) {
        locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();
        try {
            resources = ResourceBundle.getBundle(baseName, locale);
        } catch (MissingResourceException ex) {
            addMessage("Error: " + baseName + ".properties not found" + " Description: " + ex.getMessage());
        }
        this.ELECTR_RES = ELECTR_RES;
        if ((ELECTR_RES != null) && (ELECTR_RES.trim().length() > 0)) {
            imagePDF = "/images/pdf.png";
            ftpStyle = "visible";
        } else {
            imagePDF = "/images/blank.png";
            ftpStyle = "hidden";
        }
    }

    public String getREPO_RES() {
        return REPO_RES;
    }

    public void setREPO_RES(String REPO_RES) {
        locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();
        try {
            resources = ResourceBundle.getBundle(baseName, locale);
        } catch (MissingResourceException ex) {
            addMessage("Error: " + baseName + ".properties not found" + " Description: " + ex.getMessage());
        }
        this.REPO_RES = REPO_RES;
        if ((REPO_RES != null) && (REPO_RES.trim().length() > 0)) {
            imageFTP = "/images/attachment.png";
            repoStyle = "visible";
        } else {
            imageFTP = "/images/blank.png";
            repoStyle = "hidden";
        }
    }

    public String getCARD_AUTHOR() {
        return CARD_AUTHOR;
    }

    public void setCARD_AUTHOR(String CARD_AUTHOR) {
        this.CARD_AUTHOR = CARD_AUTHOR;
    }

    public String getBASIC_DESCRIPTION() {
        return BASIC_DESCRIPTION;
    }

    public void setBASIC_DESCRIPTION(String BASIC_DESCRIPTION) {
        this.BASIC_DESCRIPTION = BASIC_DESCRIPTION;
    }

    public String getSTATUS() {
        return STATUS;
    }

    public void setSTATUS(String STATUS) {
        this.STATUS = STATUS;
    }

    public Integer getROOTID() {
        return ROOTID;
    }

    public void setROOTID(Integer ROOTID) {
        locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();
        try {
            resources = ResourceBundle.getBundle(baseName, locale);
        } catch (MissingResourceException ex) {
            addMessage("Error: " + baseName + ".properties not found" + " Description: " + ex.getMessage());
        }
        this.ROOTID = ROOTID;
        switch (ROOTID) {
            case 1:
                imageURL = "/images/onebook.png";
                resource = resources.getString("facesconfig.resource[1]");
                break;
            case 2:
                imageURL = "/images/mvolume.png";
                resource = resources.getString("facesconfig.resource[2]");
                break;
            case 3:
                imageURL = "/images/subserie.png";
                resource = resources.getString("facesconfig.resource[6]");
                break;
            case 4:
                imageURL = "/images/serial.png";
                resource = resources.getString("facesconfig.resource[5]");
                break;
            case 5:
                imageURL = "/images/mpart.png";
                resource = resources.getString("facesconfig.resource[3]");
                break;
            case 6:
                imageURL = "/images/analytical.png";
                resource = resources.getString("facesconfig.resource[8]");
                break;
            case 7:
                imageURL = "/images/number.png";
                resource = resources.getString("facesconfig.resource[7]");
                break;
            case 8:
                imageURL = "/images/volume.png";
                resource = resources.getString("facesconfig.resource[4]");
                break;
            case 11:
                imageURL = "/images/ebook.png";
                resource = resources.getString("facesconfig.resource[9]");
                break;
            default:
                imageURL = "/images/blank.png";
        }
    }

    public Integer getRECNO_2() {
        return RECNO_2;
    }

    public void setRECNO_2(Integer RECNO_2) {
        this.RECNO_2 = RECNO_2;
    }

    public Integer getENCLOSURE() {
        return ENCLOSURE;
    }

    public void setENCLOSURE(Integer ENCLOSURE) {
        this.ENCLOSURE = ENCLOSURE;
    }

    public String getBDATE() {
        return BDATE;
    }

    public void setBDATE(String BDATE) {
        this.BDATE = BDATE;
    }

    public String getFtpStyle() {
        return ftpStyle;
    }

    public void setFtpStyle(String ftpStyle) {
        this.ftpStyle = ftpStyle;
    }

    public String getRepoStyle() {
        return repoStyle;
    }

    public void setRepoStyle(String repoStyle) {
        this.repoStyle = repoStyle;
    }

    public String getImagePDF() {
        return imagePDF;
    }

    public void setImagePDF(String imagePDF) {
        this.imagePDF = imagePDF;
    }
    
    public String getImageFTP() {
        return imageFTP;
    }

    public void setImageFTP(String imageFTP) {
        this.imageFTP = imageFTP;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getResource() {
        return resource;
    }

    public void setResource(String resource) {
        this.resource = resource;
    }
}
