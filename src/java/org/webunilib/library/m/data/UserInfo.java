package org.webunilib.library.m.data;

/**
 * Copyright © 2009 Jet Thoughts, Ltd. All rights reserved.
 *
 * @author Kirillov A. A.
 */
public class UserInfo implements java.io.Serializable {

    private String firstname;
    private String lastname;
    private String password;
    private String state;
    private String submit;
    private String username;
    private Integer id;

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getState() {
        return state;
    }

    public void setSubmit(String submit) {
        this.submit = submit;
    }

    public String getSubmit() {
        return submit;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
