/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.webunilib.library.vc;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Andrew Kirillov
 */
@WebServlet("/files/*")
public class Files extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
  
        File file = new File("/srv/ftp/lib", request.getParameter("id"));
        ServletOutputStream out;

        response.setContentType("application/pdf");
        //set a constant for a name to avoid cache and duplicate files on server 
        response.setHeader("Content-Disposition", "filename=fileName_" + (new Date()).getTime() + ".pdf");

        response.setHeader("Content-Transfer-Encoding", "binary;");
        response.setHeader("Pragma", " ");
        response.setHeader("Cache-Control", " ");
        response.setHeader("Pragma", "no-cache;");
        response.setDateHeader("Expires", new java.util.Date().getTime());
        //Here you will have to pass to total lengh of bytes
        //as I used a file I called length method
        response.setContentLength((int) file.length());

        out = response.getOutputStream();
        try {
            byte[] buf = new byte[4096];
            InputStream is = new FileInputStream(file);
            int c = 0;
            while ((c = is.read(buf, 0, buf.length)) > 0) {
                out.write(buf, 0, c);
            }
        } finally {
            out.flush();
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
