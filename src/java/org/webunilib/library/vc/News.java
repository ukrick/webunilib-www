package org.webunilib.library.vc;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.*;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.webunilib.library.m.data.InventoryInfo;
import org.webunilib.library.m.data.Records;
import org.webunilib.library.m.db.DbAccess;

/**
 * Copyright © 2009 Jet Thoughts, Ltd. All rights reserved.
 *
 * @author Kirillov A. A.
 */
public class News extends HttpServlet {

    /**
     * Servlet location.
     */
    protected String url;
    protected String title;
    protected String date;
    protected Locale locale;
    protected final String baseName = "org.webunilib.library.bundle";
    protected transient ResourceBundle resources;
    protected Integer bookId;
    protected ArrayList books;
    protected ArrayList records;
    protected DbAccess cartRecords;
    protected DbAccess cartNewBooks;

    /**
     * Get FacesContext.
     *
     * @return FacesContext
     */
    public static FacesContext getFacesContext() {
        return FacesContext.getCurrentInstance();
    }

    /**
     * Method to create a redirect URL.
     *
     * @return a URL to redirect to news servlet
     */
    public String getUrl() {
        FacesContext facesContext = getFacesContext();
        ExternalContext externalContext = facesContext.getExternalContext();
        url = ((HttpServletRequest) externalContext.getRequest()).getRequestURL().toString();
        StringBuilder newUrlBuilder = new StringBuilder();
        newUrlBuilder.append(url.substring(0, url.lastIndexOf(":8")));
        newUrlBuilder.append("/");
        String targetPageUrl = url.startsWith("/") ? url : "news/rss.xml";
        newUrlBuilder.append(targetPageUrl);
        return newUrlBuilder.toString();
    }

    /**
     * Set the value of url.
     *
     * @param url new value of url
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * Get the value of title.
     *
     * @return the value of title
     */
    public String getTitle() {
        locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();
        try {
            resources = ResourceBundle.getBundle(baseName, locale);
        } catch (MissingResourceException ex) {
            addMessage("Error: " + baseName + ".properties not found" + " Description: " + ex.getMessage());
        }
        title = resources.getString("news.bar.title");
        return title;
    }

    /**
     * Set the value of title.
     *
     * @param title new value of title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Get the value of date.
     *
     * @return the value of date
     */
    public String getDate() {
        date = new SimpleDateFormat("EEE d MMM yyyy HH:mm:ss").format(new Date());
        return date;
    }

    /**
     * Set the value of date.
     *
     * @param date new value of date
     */
    public void setDate(String date) {
        this.date = date;
    }

    private void addMessage(String message) {
        FacesContext context = FacesContext.getCurrentInstance();
        FacesMessage msg = new FacesMessage(message);
        context.addMessage(null, msg);
    }

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        locale = request.getLocale();
        try {
            resources = ResourceBundle.getBundle(baseName, locale);
        } catch (MissingResourceException ex) {
            addMessage("Error: " + baseName + ".properties not found" + " Description: " + ex.getMessage());
        }
        bookId = 0;
        books = new java.util.ArrayList();
        records = new java.util.ArrayList();
        cartRecords = new DbAccess();
        cartNewBooks = new DbAccess();
        Records[] dataRecords;
        InventoryInfo[] dataBooks;
        response.setContentType("text/xml;charset=UTF-8");
        PrintWriter out = response.getWriter();
        StringBuilder xml = new StringBuilder();
        xml.append("<rss version=\"2.0\">")
                .append("<channel>")
                .append("<title>").append(resources.getString("news.title")).append("</title>")
                .append("<link>").append(request.getRequestURL()).append("</link>")
                .append("<description>").append(resources.getString("news.description")).append("</description>")
                .append("<language>").append(locale.getDisplayName()).append("</language>")
                .append("<lastBuildDate>").append(this.getDate()).append("</lastBuildDate>")
                .append("<copyright>").append(resources.getString("news.copyright")).append("</copyright>")
                .append("<managingEditor>").append(resources.getString("news.managing.editor")).append("</managingEditor>")
                .append("<webMaster>").append(resources.getString("news.web.master")).append("</webMaster>")
                .append("<generator>").append(resources.getString("news.generator")).append("</generator>")
                .append("<skipDays>").append(resources.getString("news.skip.days")).append("</skipDays>");
        try {
            // getBooksRecords
            dataRecords = cartRecords.getBooksRecords();
            if (dataRecords != null) {
                records.addAll(Arrays.asList(dataRecords));
                for (Records rds : dataRecords) {
                    bookId = (Integer) rds.getRecords();
                    dataBooks = cartNewBooks.getNewRecords(bookId);
                    if (dataBooks != null) {
                        for (InventoryInfo inv : dataBooks) {
                            xml.append("<item>")
                                    .append("<title>").append(inv.getBASIC_DESCRIPTION()).append("</title>")
                                    .append("<link>")
                                    .append(request.getRequestURL().substring(0, request.getRequestURL().lastIndexOf("news"))).append("newbook.jsf?id=").append(inv.getRECNO_2())
                                    .append("</link>")
                                    .append("<description>").append(inv.getNOTE()).append("</description>")
                                    .append("<guid>").append(inv.getRECNO_2()).append("</guid>")
                                    .append("<pubDate>").append(inv.getBDATE()).append("</pubDate>")
                                    .append("<category>").append(inv.getSUBJ_CATEGORY()).append("</category>")
                                    .append("</item>");
                        }
                    }
                }
            }
        } catch (Exception ex) {
//            addMessage(resources.getString("news.error") + ": " + ex.getMessage());
        }
        xml.append("</channel>")
                .append("</rss>");
        try {
            out.println(xml.toString());
        } finally {
            out.flush();
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
