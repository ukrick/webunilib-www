package org.webunilib.library.vc;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import org.webunilib.library.m.data.InventoryInfo;
import org.webunilib.library.m.db.DbAccess;

/**
 * Copyright © 2009 Jet Thoughts, Ltd. All rights reserved.
 *
 * @author Kirillov A. A.
 */
@ManagedBean
@RequestScoped
public class Inventory implements Serializable {

    protected DbAccess cartCard;
    protected DbAccess cartInventory;
    protected List card;
    protected List copies;
    protected Integer inventoryBookId;

    /**
     * Creates a new instance of Inventory
     */
    public Inventory() {
        init();
    }

    public String getId() {
        FacesContext context = FacesContext.getCurrentInstance();
        Map map = context.getExternalContext().getRequestParameterMap();
        return (String) map.get("id");
    }

    private void init() {
        cartCard = new DbAccess();
        cartInventory = new DbAccess();
        card = new java.util.ArrayList();
        copies = new java.util.ArrayList();
        inventoryBookId = Integer.parseInt(this.getId());
        try {
            InventoryInfo[] cardbook;
            InventoryInfo[] inventory;
            cardbook = cartCard.getCard(inventoryBookId);
            if (cardbook != null) {
                card.addAll(Arrays.asList(cardbook));
            }
            inventory = cartInventory.getInventory(inventoryBookId);
            if (inventory != null) {
                copies.addAll(Arrays.asList(inventory));
            }
        } catch (Exception ex) {
            addMessage("Ошибка при поиске : " + ex.getMessage());
        }
    }

    private void addMessage(String message) {
        FacesContext context = FacesContext.getCurrentInstance();
        FacesMessage msg = new FacesMessage(message);
        context.addMessage(null, msg);
    }

    public List getCard() {
        return card;
    }

    public List getCopies() {
        return copies;
    }
}
