package org.webunilib.library.vc;

/**
 * Copyright © 2009 Jet Thoughts, Ltd. All rights reserved.
 *
 * @author Kirillov A. A.
 */
import java.text.SimpleDateFormat;
import java.util.*;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

@ManagedBean
@RequestScoped
public class FacesConfig {

    private ArrayList choicesSearchType;
    private String oneChoiceSearchType;
    private Object oneValueSearchType;
    private SelectItem[] selectsSearchType;
    private ArrayList choicesRecords;
    private String oneChoiceRecords;
    private Object oneValueRecords;
    private SelectItem[] selectsRecords;
    private ArrayList choicesResource;
    private String oneChoiceResource;
    private Object oneValueResource;
    private SelectItem[] selectsResource;
    private ArrayList choicesDocument;
    private String oneChoiceDocument;
    private Object oneValueDocument;
    private SelectItem[] selectsDocument;
    private ArrayList choicesDocType;
    private String oneChoiceDocType;
    private Object oneValueDocType;
    private SelectItem[] selectsDocType;
    private ArrayList choicesDocForm;
    private String oneChoiceDocForm;
    private Object oneValueDocForm;
    private SelectItem[] selectsDocForm;
    private ArrayList choicesCategories;
    private String oneChoiceCategories;
    private Object oneValueCategories;
    private SelectItem[] selectsCategories;
    private Locale locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();
    protected final String baseName = "org.webunilib.library.bundle";
    protected transient ResourceBundle resources;
    private String dateFrom;
    private String dateTo;

    /**
     * Creates a new instance of FacesConfig
     */
    public FacesConfig() {
        init();
    }

    private void init() {
        try {
            resources = ResourceBundle.getBundle(baseName, locale);
        } catch (MissingResourceException ex) {
            addMessage("Error: " + baseName + ".properties not found" + " Description: " + ex.getMessage());
        }
        try {
            choicesSearchType = new ArrayList();
            choicesRecords = new ArrayList();
            choicesResource = new ArrayList();
            choicesDocument = new ArrayList();
            choicesDocType = new ArrayList();
            choicesDocForm = new ArrayList();
            choicesCategories = new ArrayList();
            selectsSearchType = new SelectItem[12];
            selectsSearchType[0] = new SelectItem("1", resources.getString("facesconfig.searchType[0]"));
            selectsSearchType[1] = new SelectItem("2", resources.getString("facesconfig.searchType[1]"));
            selectsSearchType[2] = new SelectItem("3", resources.getString("facesconfig.searchType[2]"));
            selectsSearchType[3] = new SelectItem("4", resources.getString("facesconfig.searchType[3]"));
            selectsSearchType[4] = new SelectItem("5", resources.getString("facesconfig.searchType[4]"));
            selectsSearchType[5] = new SelectItem("6", resources.getString("facesconfig.searchType[5]"));
            selectsSearchType[6] = new SelectItem("7", resources.getString("facesconfig.searchType[6]"));
            selectsSearchType[7] = new SelectItem("8", resources.getString("facesconfig.searchType[7]"));
            selectsSearchType[8] = new SelectItem("9", resources.getString("facesconfig.searchType[8]"));
            selectsSearchType[9] = new SelectItem("10", resources.getString("facesconfig.searchType[9]"));
            selectsSearchType[10] = new SelectItem("11", resources.getString("facesconfig.searchType[10]"));
            selectsSearchType[11] = new SelectItem("12", resources.getString("facesconfig.searchType[11]"));
            selectsRecords = new SelectItem[3];
            selectsRecords[0] = new SelectItem("1", resources.getString("facesconfig.records[0]"));
            selectsRecords[1] = new SelectItem("2", resources.getString("facesconfig.records[1]"));
            selectsRecords[2] = new SelectItem("3", resources.getString("facesconfig.records[2]"));
            selectsResource = new SelectItem[10];
            selectsResource[0] = new SelectItem("1", resources.getString("facesconfig.resource[0]"));
            selectsResource[1] = new SelectItem("2", resources.getString("facesconfig.resource[1]"));
            selectsResource[2] = new SelectItem("3", resources.getString("facesconfig.resource[2]"));
            selectsResource[3] = new SelectItem("4", resources.getString("facesconfig.resource[3]"));
            selectsResource[4] = new SelectItem("5", resources.getString("facesconfig.resource[4]"));
            selectsResource[5] = new SelectItem("6", resources.getString("facesconfig.resource[5]"));
            selectsResource[6] = new SelectItem("7", resources.getString("facesconfig.resource[6]"));
            selectsResource[7] = new SelectItem("8", resources.getString("facesconfig.resource[7]"));
            selectsResource[8] = new SelectItem("9", resources.getString("facesconfig.resource[8]"));
            selectsResource[9] = new SelectItem("10", resources.getString("facesconfig.resource[9]"));
            selectsDocument = new SelectItem[4];
            selectsDocument[0] = new SelectItem("1", resources.getString("facesconfig.document[0]"));
            selectsDocument[1] = new SelectItem("2", resources.getString("facesconfig.document[1]"));
            selectsDocument[2] = new SelectItem("3", resources.getString("facesconfig.document[2]"));
            selectsDocument[3] = new SelectItem("4", resources.getString("facesconfig.document[3]"));
            selectsDocType = new SelectItem[6];
            selectsDocType[0] = new SelectItem("1", resources.getString("facesconfig.docType[0]"));
            selectsDocType[1] = new SelectItem("2", resources.getString("facesconfig.docType[1]"));
            selectsDocType[2] = new SelectItem("3", resources.getString("facesconfig.docType[2]"));
            selectsDocType[3] = new SelectItem("4", resources.getString("facesconfig.docType[3]"));
            selectsDocType[4] = new SelectItem("5", resources.getString("facesconfig.docType[4]"));
            selectsDocType[5] = new SelectItem("6", resources.getString("facesconfig.docType[5]"));
            selectsDocForm = new SelectItem[8];
            selectsDocForm[0] = new SelectItem("1", resources.getString("facesconfig.docForm[0]"));
            selectsDocForm[1] = new SelectItem("2", resources.getString("facesconfig.docForm[1]"));
            selectsDocForm[2] = new SelectItem("3", resources.getString("facesconfig.docForm[2]"));
            selectsDocForm[3] = new SelectItem("4", resources.getString("facesconfig.docForm[3]"));
            selectsDocForm[4] = new SelectItem("5", resources.getString("facesconfig.docForm[4]"));
            selectsDocForm[5] = new SelectItem("6", resources.getString("facesconfig.docForm[5]"));
            selectsDocForm[6] = new SelectItem("7", resources.getString("facesconfig.docForm[6]"));
            selectsDocForm[7] = new SelectItem("8", resources.getString("facesconfig.docForm[7]"));
            selectsCategories = new SelectItem[10];
            selectsCategories[0] = new SelectItem("1", resources.getString("facesconfig.categories[0]"));
            selectsCategories[1] = new SelectItem("2", resources.getString("facesconfig.categories[1]"));
            selectsCategories[2] = new SelectItem("3", resources.getString("facesconfig.categories[2]"));
            selectsCategories[3] = new SelectItem("4", resources.getString("facesconfig.categories[3]"));
            selectsCategories[4] = new SelectItem("5", resources.getString("facesconfig.categories[4]"));
            selectsCategories[5] = new SelectItem("6", resources.getString("facesconfig.categories[5]"));
            selectsCategories[6] = new SelectItem("7", resources.getString("facesconfig.categories[6]"));
            selectsCategories[7] = new SelectItem("8", resources.getString("facesconfig.categories[7]"));
            selectsCategories[8] = new SelectItem("9", resources.getString("facesconfig.categories[8]"));
            selectsCategories[9] = new SelectItem("10", resources.getString("facesconfig.categories[9]"));
            FacesContext context = FacesContext.getCurrentInstance();
            Map requestMap = context.getExternalContext().getRequestMap();
            requestMap.put("image_resource", "/images/blank.png");
            requestMap.put("title_resource", "");
            requestMap.put("locale", locale);
        } catch (Exception ex) {
            addMessage("Error: " + ex.getMessage());
        }
    }

    private void addMessage(String message) {
        FacesContext context = FacesContext.getCurrentInstance();
        FacesMessage msg = new FacesMessage(message);
        context.addMessage(null, msg);
    }

    /**
     * Get the value of dateTo
     *
     * @return the value of dateTo
     */
    public String getDateTo() {
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
        dateTo = formatter.format(date);
        return dateTo;
    }

    /**
     * Get the value of dateFrom
     *
     * @return the value of dateFrom
     */
    public String getDateFrom() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.MONTH, -1);
        SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
        dateFrom = formatter.format(calendar.getTime());
        return dateFrom;
    }
    /**
     * Value of searchText
     */
    protected String searchText;

    /**
     * Get the value of searchText
     *
     * @return the value of searchText
     */
    public String getSearchText() {
        return searchText;
    }

    /**
     * Set the value of searchText
     *
     * @param searchText new value of searchText
     */
    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }
    /**
     * URL for selected image
     */
    protected String imageURL;

    /**
     * Get the value of imageURL
     *
     * @return the value of imageURL
     */
    public String getImageURL() {
        return imageURL;
    }

    /**
     * Set the value of imageURL
     *
     * @param imageURL new value of imageURL
     */
    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }
    /**
     * Title for selected image
     */
    protected String title;

    /**
     * Get the value of title
     *
     * @return the value of title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Set the value of title
     *
     * @param title new value of title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Set the value of imageResource
     *
     * @param oneChoiceResource new value of imageResource
     */
    public void setImageResource(String oneChoiceResource) {
        if (oneChoiceResource.equals("1")) {
            imageURL = "/images/blank.png";
            title = "";
        } else if (oneChoiceResource.equals("2")) {
            imageURL = "/images/onebook.png";
            title = (String) selectsResource[1].getLabel();
        } else if (oneChoiceResource.equals("3")) {
            imageURL = "/images/mvolume.png";
            title = (String) selectsResource[2].getLabel();
        } else if (oneChoiceResource.equals("4")) {
            imageURL = "/images/mpart.png";
            title = (String) selectsResource[3].getLabel();
        } else if (oneChoiceResource.equals("5")) {
            imageURL = "/images/volume.png";
            title = (String) selectsResource[4].getLabel();
        } else if (oneChoiceResource.equals("6")) {
            imageURL = "/images/serial.png";
            title = (String) selectsResource[5].getLabel();
        } else if (oneChoiceResource.equals("7")) {
            imageURL = "/images/subserie.png";
            title = (String) selectsResource[6].getLabel();
        } else if (oneChoiceResource.equals("8")) {
            imageURL = "/images/number.png";
            title = (String) selectsResource[7].getLabel();
        } else if (oneChoiceResource.equals("9")) {
            imageURL = "/images/analytical.png";
            title = (String) selectsResource[8].getLabel();
        } else if (oneChoiceResource.equals("10")) {
            imageURL = "/images/ebook.png";
            title = (String) selectsResource[9].getLabel();
        }
        FacesContext context = FacesContext.getCurrentInstance();
        Map requestMap = context.getExternalContext().getRequestMap();
        requestMap.put("image_resource", imageURL);
        requestMap.put("title_resource", title);
    }

    // Methods - Getters
    public Object[] getChoicesSearchType() {
        return choicesSearchType.toArray();
    }

    public String getOneChoiceSearchType() {
        return oneChoiceSearchType;
    }

    public Object getOneValueSearchType() {
        return oneValueSearchType;
    }

    public SelectItem[] getSelectsSearchType() {
        return selectsSearchType;
    }

    // Methods - Setters
    public void setChoicesSearchType(Object[] choiceArray) {
        int len = 0;
        if (choiceArray != null) {
            len = choiceArray.length;
        }
        if (len != 0) {
            choicesSearchType.clear();
            choicesSearchType = new ArrayList(len);
            for (int k = 0; k < len; k++) {
                choicesSearchType.add(choiceArray[k]);
            }
        }
    }

    public void setOneChoiceSearchType(String oneChoiceSearchType) {
        this.oneChoiceSearchType = oneChoiceSearchType;
    }

    public void setOneValueSearchType(Object oneValueSearchType) {
        this.oneValueSearchType = oneValueSearchType;
    }

    public void setSelectsSearchType(SelectItem[] selectsSearchType) {
        this.selectsSearchType = selectsSearchType;
    }

    // Methods - Getters
    public Object[] getChoicesRecords() {
        return choicesRecords.toArray();
    }

    public String getOneChoiceRecords() {
        return oneChoiceRecords;
    }

    public Object getOneValueRecords() {
        return oneValueRecords;
    }

    public SelectItem[] getSelectsRecords() {
        return selectsRecords;
    }

    // Methods - Setters
    public void setChoicesRecords(Object[] choiceArray) {
        int len = 0;
        if (choiceArray != null) {
            len = choiceArray.length;
        }
        if (len != 0) {
            choicesRecords.clear();
            choicesRecords = new ArrayList(len);
            for (int k = 0; k < len; k++) {
                choicesRecords.add(choiceArray[k]);
            }
        }
    }

    public void setOneChoiceRecords(String oneChoiceRecords) {
        this.oneChoiceRecords = oneChoiceRecords;
    }

    public void setOneValueRecords(Object oneValueRecords) {
        this.oneValueRecords = oneValueRecords;
    }

    public void setSelectsRecords(SelectItem[] selectsRecords) {
        this.selectsRecords = selectsRecords;
    }

    // Methods - Getters
    public Object[] getChoicesResource() {
        return choicesResource.toArray();
    }

    public String getOneChoiceResource() {
        return oneChoiceResource;
    }

    public Object getOneValueResource() {
        return oneValueResource;
    }

    public SelectItem[] getSelectsResource() {
        return selectsResource;
    }

    // Methods - Setters
    public void setChoicesResource(Object[] choiceArray) {
        int len = 0;
        if (choiceArray != null) {
            len = choiceArray.length;
        }
        if (len != 0) {
            choicesResource.clear();
            choicesResource = new ArrayList(len);
            for (int k = 0; k < len; k++) {
                choicesResource.add(choiceArray[k]);
            }
        }
    }

    public void setOneChoiceResource(String oneChoiceResource) {
        this.oneChoiceResource = oneChoiceResource;
    }

    public void setOneValueResource(Object oneValueResource) {
        this.oneValueResource = oneValueResource;
    }

    public void setSelectsResource(SelectItem[] selectsResource) {
        this.selectsResource = selectsResource;
    }

    // Methods - Getters
    public Object[] getChoicesDocument() {
        return choicesDocument.toArray();
    }

    public String getOneChoiceDocument() {
        return oneChoiceDocument;
    }

    public Object getOneValueDocument() {
        return oneValueDocument;
    }

    public SelectItem[] getSelectsDocument() {
        return selectsDocument;
    }

    // Methods - Setters
    public void setChoicesDocument(Object[] choiceArray) {
        int len = 0;
        if (choiceArray != null) {
            len = choiceArray.length;
        }
        if (len != 0) {
            choicesDocument.clear();
            choicesDocument = new ArrayList(len);
            for (int k = 0; k < len; k++) {
                choicesDocument.add(choiceArray[k]);
            }
        }
    }

    public void setOneChoiceDocument(String oneChoiceDocument) {
        this.oneChoiceDocument = oneChoiceDocument;
    }

    public void setOneValueDocument(Object oneValueDocument) {
        this.oneValueDocument = oneValueDocument;
    }

    public void setSelectsDocument(SelectItem[] selectsDocument) {
        this.selectsDocument = selectsDocument;
    }

    // Methods - Getters
    public Object[] getChoicesDocType() {
        return choicesDocType.toArray();
    }

    public String getOneChoiceDocType() {
        return oneChoiceDocType;
    }

    public Object getOneValueDocType() {
        return oneValueDocType;
    }

    public SelectItem[] getSelectsDocType() {
        return selectsDocType;
    }

    // Methods - Setters
    public void setChoicesDocType(Object[] choiceArray) {
        int len = 0;
        if (choiceArray != null) {
            len = choiceArray.length;
        }
        if (len != 0) {
            choicesDocType.clear();
            choicesDocType = new ArrayList(len);
            for (int k = 0; k < len; k++) {
                choicesDocType.add(choiceArray[k]);
            }
        }
    }

    public void setOneChoiceDocType(String oneChoiceDocType) {
        this.oneChoiceDocType = oneChoiceDocType;
    }

    public void setOneValueDocType(Object oneValueDocType) {
        this.oneValueDocType = oneValueDocType;
    }

    public void setSelectsDocType(SelectItem[] selectsDocType) {
        this.selectsDocType = selectsDocType;
    }

    // Methods - Getters
    public Object[] getChoicesDocForm() {
        return choicesDocForm.toArray();
    }

    public String getOneChoiceDocForm() {
        return oneChoiceDocForm;
    }

    public Object getOneValueDocForm() {
        return oneValueDocForm;
    }

    public SelectItem[] getSelectsDocForm() {
        return selectsDocForm;
    }

    // Methods - Setters
    public void setChoicesDocForm(Object[] choiceArray) {
        int len = 0;
        if (choiceArray != null) {
            len = choiceArray.length;
        }
        if (len != 0) {
            choicesDocForm.clear();
            choicesDocForm = new ArrayList(len);
            for (int k = 0; k < len; k++) {
                choicesDocForm.add(choiceArray[k]);
            }
        }
    }

    public void setOneChoiceDocForm(String oneChoiceDocForm) {
        this.oneChoiceDocForm = oneChoiceDocForm;
    }

    public void setOneValueDocForm(Object oneValueDocForm) {
        this.oneValueDocForm = oneValueDocForm;
    }

    public void setSelectsDocForm(SelectItem[] selectsDocForm) {
        this.selectsDocForm = selectsDocForm;
    }

    // Methods - Getters
    public Object[] getChoicesCategories() {
        return choicesCategories.toArray();
    }

    public String getOneChoiceCategories() {
        return oneChoiceCategories;
    }

    public Object getOneValueCategories() {
        return oneValueCategories;
    }

    public SelectItem[] getSelectsCategories() {
        return selectsCategories;
    }

    // Methods - Setters
    public void setChoicesCategories(Object[] choiceArray) {
        int len = 0;
        if (choiceArray != null) {
            len = choiceArray.length;
        }
        if (len != 0) {
            choicesCategories.clear();
            choicesCategories = new ArrayList(len);
            for (int k = 0; k < len; k++) {
                choicesCategories.add(choiceArray[k]);
            }
        }
    }

    public void setOneChoiceCategories(String oneChoiceCategories) {
        this.oneChoiceCategories = oneChoiceCategories;
    }

    public void setOneValueCategories(Object oneValueCategories) {
        this.oneValueCategories = oneValueCategories;
    }

    public void setSelectsCategories(SelectItem[] selectsCategories) {
        this.selectsCategories = selectsCategories;
    }
}
