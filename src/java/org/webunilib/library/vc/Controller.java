package org.webunilib.library.vc;

/**
 * Copyright © 2009 Jet Thoughts, Ltd. All rights reserved.
 *
 * @author Kirillov A. A.
 */
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import org.webunilib.library.m.data.InventoryInfo;
import org.webunilib.library.m.data.Records;
import org.webunilib.library.m.db.DbAccess;

@ManagedBean
@SessionScoped
public class Controller implements Serializable {

    protected DataModel cardModel;
    protected DataModel copiesModel;
    protected DataModel volumesModel;
    protected DataModel bookModel;
    protected DataModel booksModel;
    protected DbAccess cartRecords;
    protected DbAccess cartBooks;
    protected DbAccess cartDetails;
    protected DbAccess cartVolumes;
    protected DbAccess cartCard;
    protected DbAccess cartInventory;
    protected List card;
    protected List copies;
    protected List volumes;
    protected List book;
    protected List books;
    protected List records;
    protected Integer inventoryCount;
    protected Integer volumesCount;
    protected String searchText;
    protected String searchSubject;
    protected String searchKeyword;
    protected String searchTitle;
    protected String searchAuthor;
    protected String searchPubLoc;
    protected String searchPubHouse;
    protected String searchPubDate;
    protected String searchISXN;
    protected String searchUDDK;
    protected String searchOTHK;
    protected String searchInvNum;
    protected String searchCodeStore;
    protected Integer rowId;
    protected Integer statusBook;
    protected String ftpURL;
    protected Integer bookId;
    protected Integer detailsBookId;
    protected Integer inventoryBookId;
    protected Integer volumeBookId;
    protected Integer multivolumeBookId;
    protected boolean newBooks;
    protected String dateFrom;
    protected String dateTo;
    private String searchFrom;
    private String searchTo;

    /**
     * Creates a new instance of Controller
     */
    public Controller() {
        init();
    }

    private void init() {
        try {
            first = 0;
            card = null;
            copies = null;
            volumes = null;
            book = null;
            books = null;
            records = null;
            cartRecords = new DbAccess();
            cartBooks = new DbAccess();
            cartDetails = new DbAccess();
            cartVolumes = new DbAccess();
            cartCard = new DbAccess();
            cartInventory = new DbAccess();
        } catch (Exception ex) {
            addMessage("Error: " + ex.getMessage());
        }
    }

    public void setInventoryCount(Integer inventoryCount) {
        this.inventoryCount = inventoryCount;
    }

    public Integer getInventoryCount() {
        return inventoryCount;
    }

    public void setVolumesCount(Integer volumesCount) {
        this.volumesCount = volumesCount;
    }

    public Integer getVolumesCount() {
        return volumesCount;
    }

    public String searchInventory() {
        searchSubject = new String();
        searchKeyword = new String();
        searchTitle = new String();
        searchAuthor = new String();
        searchPubLoc = new String();
        searchPubHouse = new String();
        searchPubDate = new String();
        searchISXN = new String();
        searchUDDK = new String();
        searchOTHK = new String();
        searchInvNum = new String();
        searchCodeStore = new String();
        searchFrom = new String();
        searchTo = new String();
        try {
            this.executionQuery();
        } catch (Exception ex) {
            addMessage("Ошибка при поиске : " + ex.getMessage());
            return "error";
        }
        searchText = new String();
        first = 0;
        return "searchresults";
    }

    public void executionQuery() {
        if (searchType.equals("1")) {
            if (searchText.indexOf("%") < 0) {
                searchSubject = "" + searchText + "";
                this.getDates(dateFrom, dateTo);
                this.getSearchRecordsBySubject(searchSubject,
                        filteredResource,
                        filteredDocument,
                        filteredDoctype,
                        filteredDocform,
                        filteredCategories, searchFrom, searchTo);
            }
        } else if (searchType.equals("2")) {
            if (searchText.indexOf("%") < 0) {
                searchKeyword = "" + searchText + "";
                this.getDates(dateFrom, dateTo);
                this.getSearchRecordsByTitle(searchKeyword,
                        filteredResource,
                        filteredDocument,
                        filteredDoctype,
                        filteredDocform,
                        filteredCategories, searchFrom, searchTo);
            }
        } else if (searchType.equals("3")) {
            if (searchText.indexOf("%") < 0) {
                searchTitle = "" + searchText + "";
                this.getDates(dateFrom, dateTo);
                this.getSearchRecordsByTitle(searchTitle,
                        filteredResource,
                        filteredDocument,
                        filteredDoctype,
                        filteredDocform,
                        filteredCategories, searchFrom, searchTo);
            }
        } else if (searchType.equals("4")) {
            if (searchText.indexOf("%") < 0) {
                searchAuthor = "" + searchText + "";
                this.getDates(dateFrom, dateTo);
                this.getSearchRecordsByAuthor(searchAuthor,
                        filteredResource,
                        filteredDocument,
                        filteredDoctype,
                        filteredDocform,
                        filteredCategories, searchFrom, searchTo);
            }
        } else if (searchType.equals("5")) {
            if (searchText.indexOf("%") < 0) {
                searchPubLoc = "" + searchText + "";
                this.getDates(dateFrom, dateTo);
                this.getSearchRecordsByPubLoc(searchPubLoc,
                        filteredResource,
                        filteredDocument,
                        filteredDoctype,
                        filteredDocform,
                        filteredCategories, searchFrom, searchTo);
            }
        } else if (searchType.equals("6")) {
            if (searchText.indexOf("%") < 0) {
                searchPubHouse = "" + searchText + "";
                this.getDates(dateFrom, dateTo);
                this.getSearchRecordsByPubHouse(searchPubHouse,
                        filteredResource,
                        filteredDocument,
                        filteredDoctype,
                        filteredDocform,
                        filteredCategories, searchFrom, searchTo);
            }
        } else if (searchType.equals("7")) {
            if (searchText.indexOf("%") < 0) {
                searchPubDate = "" + searchText + "";
                this.getDates(dateFrom, dateTo);
                this.getSearchRecordsByPubDate(searchPubDate,
                        filteredResource,
                        filteredDocument,
                        filteredDoctype,
                        filteredDocform,
                        filteredCategories, searchFrom, searchTo);
            }
        } else if (searchType.equals("8")) {
            if (searchText.indexOf("%") < 0) {
                searchISXN = "" + searchText + "";
                this.getDates(dateFrom, dateTo);
                this.getSearchRecordsByISXN(searchISXN,
                        filteredResource,
                        filteredDocument,
                        filteredDoctype,
                        filteredDocform,
                        filteredCategories, searchFrom, searchTo);
            }
        } else if (searchType.equals("9")) {
            if (searchText.indexOf("%") < 0) {
                searchUDDK = "" + searchText + "";
                this.getDates(dateFrom, dateTo);
                this.getSearchRecordsByUDDK(searchUDDK,
                        filteredResource,
                        filteredDocument,
                        filteredDoctype,
                        filteredDocform,
                        filteredCategories, searchFrom, searchTo);
            }
        } else if (searchType.equals("10")) {
            if (searchText.indexOf("%") < 0) {
                searchOTHK = "" + searchText + "";
                this.getDates(dateFrom, dateTo);
                this.getSearchRecordsByOTHK(searchOTHK,
                        filteredResource,
                        filteredDocument,
                        filteredDoctype,
                        filteredDocform,
                        filteredCategories, searchFrom, searchTo);
            }
        } else if (searchType.equals("11")) {
            if (searchText.indexOf("%") < 0) {
                searchInvNum = "" + searchText + "";
                this.getDates(dateFrom, dateTo);
                this.getSearchRecordsByInvNum(searchInvNum,
                        filteredResource,
                        filteredDocument,
                        filteredDoctype,
                        filteredDocform,
                        filteredCategories, searchFrom, searchTo);
            }
        } else if (searchType.equals("12")) {
            if (searchText.indexOf("%") < 0) {
                searchCodeStore = "" + searchText + "";
                this.getDates(dateFrom, dateTo);
                this.getSearchRecordsByCodeStore(searchCodeStore,
                        filteredResource,
                        filteredDocument,
                        filteredDoctype,
                        filteredDocform,
                        filteredCategories, searchFrom, searchTo);
            }
        }
    }

    private void getDates(String dateFrom, String dateTo) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy hh:mm");
        if (dateFrom == null || dateFrom.length() == 0) {
            searchFrom = "" + formatter.format(new Date(0)) + "";
        } else {
            searchFrom = "" + dateFrom + "";
        }
        if (dateTo == null || dateTo.length() == 0) {
            searchTo = "" + formatter.format(Calendar.getInstance().getTime()) + "";
        } else {
            searchTo = "" + dateTo + "";
        }
    }

    public void getSearchRecordsBySubject(String searchSubject,
            String filteredResource,
            String filteredDocument,
            String filteredDoctype,
            String filteredDocform,
            String filteredCategories, String filteredDateFrom, String filteredDateTo) {
        rowId = 0;
        ftpURL = new String();
        bookId = 0;
        inventoryCount = 0;
        books = new java.util.ArrayList();
        records = new java.util.ArrayList();
        try {
            Records[] dataRecords;
            InventoryInfo[] dataBooks;
            // getSearchRecordsBySubject
            dataRecords = cartRecords.getSearchRecordsBySubject(searchSubject,
                    filteredResource,
                    filteredDocument,
                    filteredDoctype,
                    filteredDocform,
                    filteredCategories,
                    filteredDateFrom,
                    filteredDateTo);
            if (dataRecords != null) {
                records.addAll(Arrays.asList(dataRecords));
                for (Records rds : dataRecords) {
                    bookId = (Integer) rds.getRecords();
                    dataBooks = cartBooks.getAllItems(bookId);
                    if (dataBooks != null) {
                        books.addAll(Arrays.asList(dataBooks));
                        for (int i = 0; i < books.size(); i++) {
                            InventoryInfo item = (InventoryInfo) books.get(rowId);
                            ftpURL = item.getELECTR_RES();
                        }
                        inventoryCount = new Integer(books.size());
                    }
                }
            }
        } catch (Exception ex) {
            addMessage("Ошибка при поиске : " + ex.getMessage());
        }
    }

    public void getSearchRecordsByKeyword(String searchKeyword,
            String filteredResource,
            String filteredDocument,
            String filteredDoctype,
            String filteredDocform,
            String filteredCategories, String filteredDateFrom, String filteredDateTo) {
        rowId = 0;
        ftpURL = new String();
        bookId = 0;
        inventoryCount = 0;
        books = new java.util.ArrayList();
        records = new java.util.ArrayList();
        try {
            Records[] dataRecords;
            InventoryInfo[] dataBooks;
            // getSearchRecordsByKeyword
            dataRecords = cartRecords.getSearchRecordsByKeyword(searchKeyword,
                    filteredResource,
                    filteredDocument,
                    filteredDoctype,
                    filteredDocform,
                    filteredCategories,
                    filteredDateFrom,
                    filteredDateTo);
            if (dataRecords != null) {
                records.addAll(Arrays.asList(dataRecords));
                for (Records rds : dataRecords) {
                    bookId = (Integer) rds.getRecords();
                    dataBooks = cartBooks.getAllItems(bookId);
                    if (dataBooks != null) {
                        books.addAll(Arrays.asList(dataBooks));
                        for (int i = 0; i < books.size(); i++) {
                            InventoryInfo item = (InventoryInfo) books.get(rowId);
                            ftpURL = item.getELECTR_RES();
                        }
                        inventoryCount = new Integer(books.size());
                    }
                }
            }
        } catch (Exception ex) {
            addMessage("Ошибка при поиске : " + ex.getMessage());
        }
    }

    public void getSearchRecordsByTitle(String searchTitle,
            String filteredResource,
            String filteredDocument,
            String filteredDoctype,
            String filteredDocform,
            String filteredCategories, String filteredDateFrom, String filteredDateTo) {
        rowId = 0;
        ftpURL = new String();
        bookId = 0;
        inventoryCount = 0;
        books = new java.util.ArrayList();
        records = new java.util.ArrayList();
        try {
            Records[] dataRecords;
            InventoryInfo[] dataBooks;
            // getSearchRecordsByTitle
            dataRecords = cartRecords.getSearchRecordsByTitle(searchTitle,
                    filteredResource,
                    filteredDocument,
                    filteredDoctype,
                    filteredDocform,
                    filteredCategories,
                    filteredDateFrom,
                    filteredDateTo);
            if (dataRecords != null) {
                records.addAll(Arrays.asList(dataRecords));
                for (Records rds : dataRecords) {
                    bookId = (Integer) rds.getRecords();
                    dataBooks = cartBooks.getAllItems(bookId);
                    if (dataBooks != null) {
                        books.addAll(Arrays.asList(dataBooks));
                        for (int i = 0; i < books.size(); i++) {
                            InventoryInfo item = (InventoryInfo) books.get(rowId);
                            ftpURL = item.getELECTR_RES();
                        }
                        inventoryCount = new Integer(books.size());
                    }
                }
            }
        } catch (Exception ex) {
            addMessage("Ошибка при поиске : " + ex.getMessage());
        }
    }

    public void getSearchRecordsByAuthor(String searchAuthor,
            String filteredResource,
            String filteredDocument,
            String filteredDoctype,
            String filteredDocform,
            String filteredCategories, String filteredDateFrom, String filteredDateTo) {
        rowId = 0;
        ftpURL = new String();
        bookId = 0;
        inventoryCount = 0;
        books = new java.util.ArrayList();
        records = new java.util.ArrayList();
        try {
            Records[] dataRecords;
            InventoryInfo[] dataBooks;
            // getSearchRecordsByAuthor
            dataRecords = cartRecords.getSearchRecordsByAuthor(searchAuthor,
                    filteredResource,
                    filteredDocument,
                    filteredDoctype,
                    filteredDocform,
                    filteredCategories,
                    filteredDateFrom,
                    filteredDateTo);
            if (dataRecords != null) {
                records.addAll(Arrays.asList(dataRecords));
                for (Records rds : dataRecords) {
                    bookId = (Integer) rds.getRecords();
                    dataBooks = cartBooks.getAllItems(bookId);
                    if (dataBooks != null) {
                        books.addAll(Arrays.asList(dataBooks));
                        for (int i = 0; i < books.size(); i++) {
                            InventoryInfo item = (InventoryInfo) books.get(rowId);
                            ftpURL = item.getELECTR_RES();
                        }
                        inventoryCount = new Integer(books.size());
                    }
                }
            }
        } catch (Exception ex) {
            addMessage("Ошибка при поиске : " + ex.getMessage());
        }
    }

    public void getSearchRecordsByPubLoc(String searchPubLoc,
            String filteredResource,
            String filteredDocument,
            String filteredDoctype,
            String filteredDocform,
            String filteredCategories, String filteredDateFrom, String filteredDateTo) {
        rowId = 0;
        ftpURL = new String();
        bookId = 0;
        inventoryCount = 0;
        books = new java.util.ArrayList();
        records = new java.util.ArrayList();
        try {
            Records[] dataRecords;
            InventoryInfo[] dataBooks;
            // getSearchRecordsByPubLoc
            dataRecords = cartRecords.getSearchRecordsByPubLoc(searchPubLoc,
                    filteredResource,
                    filteredDocument,
                    filteredDoctype,
                    filteredDocform,
                    filteredCategories,
                    filteredDateFrom,
                    filteredDateTo);
            if (dataRecords != null) {
                records.addAll(Arrays.asList(dataRecords));
                for (Records rds : dataRecords) {
                    bookId = (Integer) rds.getRecords();
                    dataBooks = cartBooks.getAllItems(bookId);
                    if (dataBooks != null) {
                        books.addAll(Arrays.asList(dataBooks));
                        for (int i = 0; i < books.size(); i++) {
                            InventoryInfo item = (InventoryInfo) books.get(rowId);
                            ftpURL = item.getELECTR_RES();
                        }
                        inventoryCount = new Integer(books.size());
                    }
                }
            }
        } catch (Exception ex) {
            addMessage("Ошибка при поиске : " + ex.getMessage());
        }
    }

    public void getSearchRecordsByPubHouse(String searchPubHouse,
            String filteredResource,
            String filteredDocument,
            String filteredDoctype,
            String filteredDocform,
            String filteredCategories, String filteredDateFrom, String filteredDateTo) {
        rowId = 0;
        ftpURL = new String();
        bookId = 0;
        inventoryCount = 0;
        books = new java.util.ArrayList();
        records = new java.util.ArrayList();
        try {
            Records[] dataRecords;
            InventoryInfo[] dataBooks;
            // getSearchRecordsByPubHouse
            dataRecords = cartRecords.getSearchRecordsByPubHouse(searchPubHouse,
                    filteredResource,
                    filteredDocument,
                    filteredDoctype,
                    filteredDocform,
                    filteredCategories,
                    filteredDateFrom,
                    filteredDateTo);
            if (dataRecords != null) {
                records.addAll(Arrays.asList(dataRecords));
                for (Records rds : dataRecords) {
                    bookId = (Integer) rds.getRecords();
                    dataBooks = cartBooks.getAllItems(bookId);
                    if (dataBooks != null) {
                        books.addAll(Arrays.asList(dataBooks));
                        for (int i = 0; i < books.size(); i++) {
                            InventoryInfo item = (InventoryInfo) books.get(rowId);
                            ftpURL = item.getELECTR_RES();
                        }
                        inventoryCount = new Integer(books.size());
                    }
                }
            }
        } catch (Exception ex) {
            addMessage("Ошибка при поиске : " + ex.getMessage());
        }
    }

    public void getSearchRecordsByPubDate(String searchPubDate,
            String filteredResource,
            String filteredDocument,
            String filteredDoctype,
            String filteredDocform,
            String filteredCategories, String filteredDateFrom, String filteredDateTo) {
        rowId = 0;
        ftpURL = new String();
        bookId = 0;
        inventoryCount = 0;
        books = new java.util.ArrayList();
        records = new java.util.ArrayList();
        try {
            Records[] dataRecords;
            InventoryInfo[] dataBooks;
            // getSearchRecordsByPubDate
            dataRecords = cartRecords.getSearchRecordsByPubDate(searchPubDate,
                    filteredResource,
                    filteredDocument,
                    filteredDoctype,
                    filteredDocform,
                    filteredCategories,
                    filteredDateFrom,
                    filteredDateTo);
            if (dataRecords != null) {
                records.addAll(Arrays.asList(dataRecords));
                for (Records rds : dataRecords) {
                    bookId = (Integer) rds.getRecords();
                    dataBooks = cartBooks.getAllItems(bookId);
                    if (dataBooks != null) {
                        books.addAll(Arrays.asList(dataBooks));
                        for (int i = 0; i < books.size(); i++) {
                            InventoryInfo item = (InventoryInfo) books.get(rowId);
                            ftpURL = item.getELECTR_RES();
                        }
                        inventoryCount = new Integer(books.size());
                    }
                }
            }
        } catch (Exception ex) {
            addMessage("Ошибка при поиске : " + ex.getMessage());
        }
    }

    public void getSearchRecordsByISXN(String searchISXN,
            String filteredResource,
            String filteredDocument,
            String filteredDoctype,
            String filteredDocform,
            String filteredCategories, String filteredDateFrom, String filteredDateTo) {
        rowId = 0;
        ftpURL = new String();
        bookId = 0;
        inventoryCount = 0;
        books = new java.util.ArrayList();
        records = new java.util.ArrayList();
        try {
            Records[] dataRecords;
            InventoryInfo[] dataBooks;
            // getSearchRecordsByISXN
            dataRecords = cartRecords.getSearchRecordsByISXN(searchISXN,
                    filteredResource,
                    filteredDocument,
                    filteredDoctype,
                    filteredDocform,
                    filteredCategories,
                    filteredDateFrom,
                    filteredDateTo);
            if (dataRecords != null) {
                records.addAll(Arrays.asList(dataRecords));
                for (Records rds : dataRecords) {
                    bookId = (Integer) rds.getRecords();
                    dataBooks = cartBooks.getAllItems(bookId);
                    if (dataBooks != null) {
                        books.addAll(Arrays.asList(dataBooks));
                        for (int i = 0; i < books.size(); i++) {
                            InventoryInfo item = (InventoryInfo) books.get(rowId);
                            ftpURL = item.getELECTR_RES();
                        }
                        inventoryCount = new Integer(books.size());
                    }
                }
            }
        } catch (Exception ex) {
            addMessage("Ошибка при поиске : " + ex.getMessage());
        }
    }

    public void getSearchRecordsByUDDK(String searchUDDK,
            String filteredResource,
            String filteredDocument,
            String filteredDoctype,
            String filteredDocform,
            String filteredCategories, String filteredDateFrom, String filteredDateTo) {
        rowId = 0;
        ftpURL = new String();
        bookId = 0;
        inventoryCount = 0;
        books = new java.util.ArrayList();
        records = new java.util.ArrayList();
        try {
            Records[] dataRecords;
            InventoryInfo[] dataBooks;
            // getSearchRecordsByUDDK
            dataRecords = cartRecords.getSearchRecordsByUDDK(searchUDDK,
                    filteredResource,
                    filteredDocument,
                    filteredDoctype,
                    filteredDocform,
                    filteredCategories,
                    filteredDateFrom,
                    filteredDateTo);
            if (dataRecords != null) {
                records.addAll(Arrays.asList(dataRecords));
                for (Records rds : dataRecords) {
                    bookId = (Integer) rds.getRecords();
                    dataBooks = cartBooks.getAllItems(bookId);
                    if (dataBooks != null) {
                        books.addAll(Arrays.asList(dataBooks));
                        for (int i = 0; i < books.size(); i++) {
                            InventoryInfo item = (InventoryInfo) books.get(rowId);
                            ftpURL = item.getELECTR_RES();
                        }
                        inventoryCount = new Integer(books.size());
                    }
                }
            }
        } catch (Exception ex) {
            addMessage("Ошибка при поиске : " + ex.getMessage());
        }
    }

    public void getSearchRecordsByOTHK(String searchOTHK,
            String filteredResource,
            String filteredDocument,
            String filteredDoctype,
            String filteredDocform,
            String filteredCategories, String filteredDateFrom, String filteredDateTo) {
        rowId = 0;
        ftpURL = new String();
        bookId = 0;
        inventoryCount = 0;
        books = new java.util.ArrayList();
        records = new java.util.ArrayList();
        try {
            Records[] dataRecords;
            InventoryInfo[] dataBooks;
            // getSearchRecordsByOTHK
            dataRecords = cartRecords.getSearchRecordsByOTHK(searchOTHK,
                    filteredResource,
                    filteredDocument,
                    filteredDoctype,
                    filteredDocform,
                    filteredCategories,
                    filteredDateFrom,
                    filteredDateTo);
            if (dataRecords != null) {
                records.addAll(Arrays.asList(dataRecords));
                for (Records rds : dataRecords) {
                    bookId = (Integer) rds.getRecords();
                    dataBooks = cartBooks.getAllItems(bookId);
                    if (dataBooks != null) {
                        books.addAll(Arrays.asList(dataBooks));
                        for (int i = 0; i < books.size(); i++) {
                            InventoryInfo item = (InventoryInfo) books.get(rowId);
                            ftpURL = item.getELECTR_RES();
                        }
                        inventoryCount = new Integer(books.size());
                    }
                }
            }
        } catch (Exception ex) {
            addMessage("Ошибка при поиске : " + ex.getMessage());
        }
    }

    public void getSearchRecordsByInvNum(String searchInvNum,
            String filteredResource,
            String filteredDocument,
            String filteredDoctype,
            String filteredDocform,
            String filteredCategories, String filteredDateFrom, String filteredDateTo) {
        rowId = 0;
        ftpURL = new String();
        bookId = 0;
        inventoryCount = 0;
        books = new java.util.ArrayList();
        records = new java.util.ArrayList();
        try {
            Records[] dataRecords;
            InventoryInfo[] dataBooks;
            // getSearchRecordsByInvNum
            dataRecords = cartRecords.getSearchRecordsByInvNum(searchInvNum,
                    filteredResource,
                    filteredDocument,
                    filteredDoctype,
                    filteredDocform,
                    filteredCategories,
                    filteredDateFrom,
                    filteredDateTo);
            if (dataRecords != null) {
                records.addAll(Arrays.asList(dataRecords));
                for (Records rds : dataRecords) {
                    bookId = (Integer) rds.getRecords();
                    dataBooks = cartBooks.getAllItems(bookId);
                    if (dataBooks != null) {
                        books.addAll(Arrays.asList(dataBooks));
                        for (int i = 0; i < books.size(); i++) {
                            InventoryInfo item = (InventoryInfo) books.get(rowId);
                            ftpURL = item.getELECTR_RES();
                        }
                        inventoryCount = new Integer(books.size());
                    }
                }
            }
        } catch (Exception ex) {
            addMessage("Ошибка при поиске : " + ex.getMessage());
        }
    }

    public void getSearchRecordsByCodeStore(String searchCodeStore,
            String filteredResource,
            String filteredDocument,
            String filteredDoctype,
            String filteredDocform,
            String filteredCategories, String filteredDateFrom, String filteredDateTo) {
        rowId = 0;
        ftpURL = new String();
        bookId = 0;
        inventoryCount = 0;
        books = new java.util.ArrayList();
        records = new java.util.ArrayList();
        try {
            Records[] dataRecords;
            InventoryInfo[] dataBooks;
            // getSearchRecordsByCodeStore
            dataRecords = cartRecords.getSearchRecordsByCodeStore(searchCodeStore,
                    filteredResource,
                    filteredDocument,
                    filteredDoctype,
                    filteredDocform,
                    filteredCategories,
                    filteredDateFrom,
                    filteredDateTo);
            if (dataRecords != null) {
                records.addAll(Arrays.asList(dataRecords));
                for (Records rds : dataRecords) {
                    bookId = (Integer) rds.getRecords();
                    dataBooks = cartBooks.getAllItems(bookId);
                    if (dataBooks != null) {
                        books.addAll(Arrays.asList(dataBooks));
                        for (int i = 0; i < books.size(); i++) {
                            InventoryInfo item = (InventoryInfo) books.get(rowId);
                            ftpURL = item.getELECTR_RES();
                        }
                        inventoryCount = new Integer(books.size());
                    }
                }
            }
        } catch (Exception ex) {
            addMessage("Ошибка при поиске : " + ex.getMessage());
        }
    }

    public String inventory() {
        inventoryBookId = 0;
        card = new java.util.ArrayList();
        copies = new java.util.ArrayList();
        try {
            InventoryInfo[] cardbook;
            InventoryInfo[] inventory;
            inventoryBookId = this.getDetailsBookId();
            cardbook = cartCard.getCard(inventoryBookId);
            if (cardbook != null) {
                card.addAll(Arrays.asList(cardbook));
            }
            inventory = cartInventory.getInventory(inventoryBookId);
            if (inventory != null) {
                copies.addAll(Arrays.asList(inventory));
            }
        } catch (Exception ex) {
            addMessage("Ошибка при поиске : " + ex.getMessage());
            return "error";
        }
        return "inventory";
    }

    public String volumes() {
        multivolumeBookId = 0;
        bookId = 0;
        volumesCount = 0;
        card = new java.util.ArrayList();
        records = new java.util.ArrayList();
        volumes = new java.util.ArrayList();
        try {
            Records[] dataRecords;
            InventoryInfo[] cardbook;
            InventoryInfo[] dataBooks;
            // getDetailsBookId
            multivolumeBookId = this.getDetailsBookId();
            inventoryBookId = multivolumeBookId;
            // getCard
            cardbook = cartCard.getCard(inventoryBookId);
            if (cardbook != null) {
                card.addAll(Arrays.asList(cardbook));
            }
            // getVolumes
            dataRecords = cartRecords.getVolumes(multivolumeBookId);
            if (dataRecords != null) {
                records.addAll(Arrays.asList(dataRecords));
                for (Records rds : dataRecords) {
                    bookId = (Integer) rds.getRecords();
                    dataBooks = cartVolumes.getAllItems(bookId);
                    if (dataBooks != null) {
                        volumes.addAll(Arrays.asList(dataBooks));
                        volumesCount = new Integer(volumes.size());
                    }
                }
            }
        } catch (Exception ex) {
            addMessage("Ошибка при поиске : " + ex.getMessage());
            return "error";
        }
        return "volumes";
    }

    public String edition() {
        volumeBookId = 0;
        multivolumeBookId = 0;
        bookId = 0;
        volumesCount = 0;
        card = new java.util.ArrayList();
        records = new java.util.ArrayList();
        volumes = new java.util.ArrayList();
        try {
            Records[] dataRecords;
            InventoryInfo[] cardbook;
            InventoryInfo[] dataBooks;
            // getDetailsBookId
            volumeBookId = this.getDetailsBookId();
            // getVolumesByMultiVolumesBook
            multivolumeBookId
                    = cartRecords.getSerialEditionId(volumeBookId);
            inventoryBookId = multivolumeBookId;
            // getCard
            cardbook = cartCard.getCard(inventoryBookId);
            if (cardbook != null) {
                card.addAll(Arrays.asList(cardbook));
            }
            // getVolumes
            dataRecords = cartRecords.getArticles(multivolumeBookId);
            if (dataRecords != null) {
                records.addAll(Arrays.asList(dataRecords));
                for (Records rds : dataRecords) {
                    bookId = (Integer) rds.getRecords();
                    dataBooks = cartVolumes.getAllItems(bookId);
                    if (dataBooks != null) {
                        volumes.addAll(Arrays.asList(dataBooks));
                        volumesCount = new Integer(volumes.size());
                    }
                }
            }
        } catch (Exception ex) {
            addMessage("Ошибка при поиске : " + ex.getMessage());
            return "error";
        }
        return "volumes";
    }

    public String volumesByMultiVolumesBook() {
        volumeBookId = 0;
        multivolumeBookId = 0;
        bookId = 0;
        volumesCount = 0;
        card = new java.util.ArrayList();
        records = new java.util.ArrayList();
        volumes = new java.util.ArrayList();
        statusBook = 2;    // Multi Volumes Book
        try {
            Records[] dataRecords;
            InventoryInfo[] cardbook;
            InventoryInfo[] dataBooks;
            // getDetailsBookId
            volumeBookId = this.getDetailsBookId();
            // getVolumesByMultiVolumesBook
            multivolumeBookId
                    = cartRecords.getMultiVolumesBookId(volumeBookId);
            inventoryBookId = multivolumeBookId;
            // getCard
            cardbook = cartCard.getCard(inventoryBookId);
            if (cardbook != null) {
                card.addAll(Arrays.asList(cardbook));
            }
            // getVolumes
            dataRecords = cartRecords.getVolumes(multivolumeBookId);
            if (dataRecords != null) {
                records.addAll(Arrays.asList(dataRecords));
                for (Records rds : dataRecords) {
                    bookId = (Integer) rds.getRecords();
                    dataBooks = cartVolumes.getAllItems(bookId);
                    if (dataBooks != null) {
                        volumes.addAll(Arrays.asList(dataBooks));
                        volumesCount = new Integer(volumes.size());
                    }
                }
            }
        } catch (Exception ex) {
            addMessage("Ошибка при поиске : " + ex.getMessage());
            return "error";
        }
        return "volumes";
    }

    public String newBooks() {
        searchFrom = new String();
        searchTo = new String();
        bookId = 0;
        inventoryCount = 0;
        books = new java.util.ArrayList();
        records = new java.util.ArrayList();
        try {
            Records[] dataRecords;
            InventoryInfo[] dataBooks;
            // getBooksRecords
            dataRecords = cartRecords.getBooksRecords();
            if (dataRecords != null) {
                records.addAll(Arrays.asList(dataRecords));
                for (Records rds : dataRecords) {
                    bookId = (Integer) rds.getRecords();
                    dataBooks = cartBooks.getNewBooks(bookId);
                    if (dataBooks != null) {
                        books.addAll(Arrays.asList(dataBooks));
                        inventoryCount = new Integer(books.size());
                    }
                }
            }
        } catch (Exception ex) {
            addMessage("Ошибка при поиске : " + ex.getMessage());
            return "error";
        }
        first = 0;
        rows = 10;
        return "newbooks";
    }

    public String newBooksDateRange() {
        searchFrom = new String();
        searchTo = new String();
        bookId = 0;
        inventoryCount = 0;
        books = new java.util.ArrayList();
        records = new java.util.ArrayList();
        if ((dateFrom.indexOf("%") < 0) && (dateTo.indexOf("%") < 0)) {
            searchFrom = "" + dateFrom + "";
            searchTo = "" + dateTo + "";
            try {
                Records[] dataRecords;
                InventoryInfo[] dataBooks;
                // getBooksRecords
                dataRecords = cartRecords.getBooksRecords(searchFrom, searchTo);
                if (dataRecords != null) {
                    records.addAll(Arrays.asList(dataRecords));
                    for (Records rds : dataRecords) {
                        bookId = (Integer) rds.getRecords();
                        dataBooks = cartBooks.getNewBooks(bookId);
                        if (dataBooks != null) {
                            books.addAll(Arrays.asList(dataBooks));
                            inventoryCount = new Integer(books.size());
                        }
                    }
                }
            } catch (Exception ex) {
                addMessage("Ошибка при поиске : " + ex.getMessage());
                return "error";
            }
        }
        rows = 10;
        return "newbooks";
    }

    public List getRecords() {
        return records;
    }

    public List getCard() {
        return card;
    }

    public List getCopies() {
        return copies;
    }

    public List getVolumes() {
        return volumes;
    }

    public List getBook() {
        return book;
    }

    public List getBooks() {
        return books;
    }

    public DataModel getCardModel() {
        if (cardModel == null) {
            cardModel = new ListDataModel();
        }
        cardModel.setWrappedData(card);
        return cardModel;
    }

    public DataModel getCopiesModel() {
        if (copiesModel == null) {
            copiesModel = new ListDataModel();
        }
        copiesModel.setWrappedData(copies);
        return copiesModel;
    }

    public DataModel getVolumesModel() {
        if (volumesModel == null) {
            volumesModel = new ListDataModel();
        }
        volumesModel.setWrappedData(volumes);
        return volumesModel;
    }

    public DataModel getBookModel() {
        if (bookModel == null) {
            bookModel = new ListDataModel();
        }
        bookModel.setWrappedData(book);
        return bookModel;
    }

    public DataModel getBooksModel() {
        if (booksModel == null) {
            booksModel = new ListDataModel();
        }
        booksModel.setWrappedData(books);
        return booksModel;
    }

    public Integer getBookId() {
        return bookId;
    }

    public Integer getDetailsBookId() {
        return detailsBookId;
    }

    public Integer getRowId() {
        return rowId;
    }

    public String getSearchText() {
        return searchText;
    }

    public String getDateFrom() {
        return dateFrom;
    }

    public String getDateTo() {
        return dateTo;
    }

    public String getSearchFrom() {
        return searchFrom;
    }

    public String getSearchTo() {
        return searchTo;
    }

    public String getSearchSubject() {
        return searchSubject;
    }

    public String getSearchKeyword() {
        return searchKeyword;
    }

    public String getSearchTitle() {
        return searchTitle;
    }

    public String getSearchAuthor() {
        return searchAuthor;
    }

    public String getSearchPubLoc() {
        return searchPubLoc;
    }

    public String getSearchPubHouse() {
        return searchPubHouse;
    }

    public String getSearchPubDate() {
        return searchPubDate;
    }

    public String getSearchISXN() {
        return searchISXN;
    }

    public String getSearchUDDK() {
        return searchUDDK;
    }

    public String getSearchOTHK() {
        return searchOTHK;
    }

    public String getSearchInvNum() {
        return searchInvNum;
    }

    public String getSearchCodeStore() {
        return searchCodeStore;
    }

    public Integer getStatusBook() {
        return statusBook;
    }

    public Integer getInventoryBookId() {
        return inventoryBookId;
    }

    public Integer getVolumeBookId() {
        return volumeBookId;
    }

    public Integer getMultivolumeBookId() {
        return multivolumeBookId;
    }

    public String getFtpURL() {
        return ftpURL;
    }

    public String getShowQueryByAuthor() {
        if (searchAuthor != null) {
            if (searchAuthor.length() > 0) {
                return "true";
            }
        }
        return "false";
    }

    public String getShowQueryByTitle() {
        if (searchTitle != null) {
            if (searchTitle.length() > 0) {
                return "true";
            }
        }
        return "false";
    }

    public String getShowQueryBySubject() {
        if (searchSubject != null) {
            if (searchSubject.length() > 0) {
                return "true";
            }
        }
        return "false";
    }

    public String getShowQueryByKeyword() {
        if (searchKeyword != null) {
            if (searchKeyword.length() > 0) {
                return "true";
            }
        }
        return "false";
    }

    public String getShowQueryByPubLoc() {
        if (searchPubLoc != null) {
            if (searchPubLoc.length() > 0) {
                return "true";
            }
        }
        return "false";
    }

    public String getShowQueryByPubHouse() {
        if (searchPubHouse != null) {
            if (searchPubHouse.length() > 0) {
                return "true";
            }
        }
        return "false";
    }

    public String getShowQueryByPubDate() {
        if (searchPubDate != null) {
            if (searchPubDate.length() > 0) {
                return "true";
            }
        }
        return "false";
    }

    public String getShowQueryByCodeStore() {
        if (searchCodeStore != null) {
            if (searchCodeStore.length() > 0) {
                return "true";
            }
        }
        return "false";
    }

    public String getShowQueryByInvNum() {
        if (searchInvNum != null) {
            if (searchInvNum.length() > 0) {
                return "true";
            }
        }
        return "false";
    }

    public String getShowQueryByUDDK() {
        if (searchUDDK != null) {
            if (searchUDDK.length() > 0) {
                return "true";
            }
        }
        return "false";
    }

    public String getShowQueryByOTHK() {
        if (searchOTHK != null) {
            if (searchOTHK.length() > 0) {
                return "true";
            }
        }
        return "false";
    }

    public String getShowQueryByISXN() {
        if (searchISXN != null) {
            if (searchISXN.length() > 0) {
                return "true";
            }
        }
        return "false";
    }

    public void setRecords(List records) {
        this.records = records;
    }

    public void setCard(List card) {
        this.card = card;
    }

    public void setCopies(List copies) {
        this.copies = copies;
    }

    public void setVolumes(List volumes) {
        this.volumes = volumes;
    }

    public void setBook(List book) {
        this.book = book;
    }

    public void setBooks(List books) {
        this.books = books;
    }

    public void setBookId(Integer bookId) {
        this.bookId = bookId;
    }

    public void setDetailsBookId(Integer detailsBookId) {
        this.detailsBookId = detailsBookId;
    }

    public void setRowId(Integer rowId) {
        this.rowId = rowId;
    }

    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }

    public void setDateFrom(String dateFrom) {
        this.dateFrom = dateFrom;
    }

    public void setDateTo(String dateTo) {
        this.dateTo = dateTo;
    }

    public void setSearchFrom(String searchFrom) {
        this.searchFrom = searchFrom;
    }

    public void setSearchTo(String searchTo) {
        this.searchTo = searchTo;
    }

    public void setSearchSubject(String searchSubject) {
        this.searchSubject = searchSubject;
    }

    public void setSearchKeyword(String searchKeyword) {
        this.searchKeyword = searchKeyword;
    }

    public void setSearchTitle(String searchTitle) {
        this.searchTitle = searchTitle;
    }

    public void setSearchAuthor(String searchAuthor) {
        this.searchAuthor = searchAuthor;
    }

    public void setSearchPubLoc(String searchPubLoc) {
        this.searchPubLoc = searchPubLoc;
    }

    public void setSearchPubHouse(String searchPubHouse) {
        this.searchPubHouse = searchPubHouse;
    }

    public void setSearchPubDate(String searchPubDate) {
        this.searchPubDate = searchPubDate;
    }

    public void setSearchISXN(String searchISXN) {
        this.searchISXN = searchISXN;
    }

    public void setSearchUDDK(String searchUDDK) {
        this.searchUDDK = searchUDDK;
    }

    public void setSearchOTHK(String searchOTHK) {
        this.searchOTHK = searchOTHK;
    }

    public void setSearchInvNum(String searchInvNum) {
        this.searchInvNum = searchInvNum;
    }

    public void setSearchCodeStore(String searchCodeStore) {
        this.searchCodeStore = searchCodeStore;
    }

    public void setStatusBook(Integer statusBook) {
        this.statusBook = statusBook;
    }

    public void setInventoryBookId(Integer inventoryBookId) {
        this.inventoryBookId = inventoryBookId;
    }

    public void setVolumeBookId(Integer volumeBookId) {
        this.volumeBookId = volumeBookId;
    }

    public void setMultivolumeBookId(Integer multivolumeBookId) {
        this.multivolumeBookId = multivolumeBookId;
    }

    public void setFtpURL(String ftpURL) {
        this.ftpURL = ftpURL;
    }

    private void addMessage(String message) {
        FacesContext context = FacesContext.getCurrentInstance();
        FacesMessage msg = new FacesMessage(message);
        context.addMessage(null, msg);
    }

    public String newSearch() {
        return "welcome";
    }

    public String openSearchResults() {
        return "searchresults";
    }

    public String openNewBooksResults() {
        return "newbooks";
    }

    public String openVolumes() {
        return "volumes";
    }

    public String openDetails() {
        return "details";
    }

    public String openPrintNewBooks() {
        return "printnewbooks";
    }

    public String openPrintResults() {
        return "printresults";
    }

    public String openPrintVolumes() {
        return "printvolumes";
    }

    public String openPrintDetails() {
        return "printdetails";
    }

    public String openPrintInventory() {
        return "printinventory";
    }

    public String details() {
        rowId = 0;
        ftpURL = new String();
        detailsBookId = 0;
        statusBook = 0;
        book = new java.util.ArrayList();
        try {
            InventoryInfo[] details;
            rowId = booksModel.getRowIndex();
            InventoryInfo item = (InventoryInfo) books.get(rowId);
            detailsBookId = item.getRECNO_2();
            ftpURL = item.getELECTR_RES();
            statusBook = item.getROOTID();
            details = cartDetails.getDescription(detailsBookId);
            if (details != null) {
                book.addAll(Arrays.asList(details));
            }
        } catch (Exception ex) {
            addMessage("Ошибка при поиске : " + ex.getMessage());
            return "error";
        }
        setNewBooks(false);
        return "details";
    }

    public String detailsMultiVolumesBook() {
        statusBook = 2;    // Multi Volumes Book
        book = new java.util.ArrayList();
        try {
            InventoryInfo[] details;
            detailsBookId = multivolumeBookId;
            details = cartCard.getDescription(detailsBookId);
            if (details != null) {
                book.addAll(Arrays.asList(details));
            }
        } catch (Exception ex) {
            addMessage("Ошибка при поиске : " + ex.getMessage());
            return "error";
        }
        return "details";
    }

    public String detailsSerialEdition() {
        statusBook = null;    // Serial Edition
        book = new java.util.ArrayList();
        try {
            InventoryInfo[] details;
            detailsBookId = multivolumeBookId;
            details = cartCard.getDescription(detailsBookId);
            if (details != null) {
                book.addAll(Arrays.asList(details));
            }
        } catch (Exception ex) {
            addMessage("Ошибка при поиске : " + ex.getMessage());
            return "error";
        }
        return "details";
    }

    public String detailsVolume() {
        rowId = 0;
        ftpURL = new String();
        detailsBookId = 0;
        statusBook = 0;
        book = new java.util.ArrayList();
        try {
            InventoryInfo[] details;
            rowId = volumesModel.getRowIndex();
            InventoryInfo item = (InventoryInfo) volumes.get(rowId);
            detailsBookId = item.getRECNO_2();
            ftpURL = item.getELECTR_RES();
            statusBook = item.getROOTID();
            details = cartDetails.getDescription(detailsBookId);
            if (details != null) {
                book.addAll(Arrays.asList(details));
            }
        } catch (Exception ex) {
            addMessage("Ошибка при поиске : " + ex.getMessage());
            return "error";
        }
        return "details";
    }

    public String detailsNewBook() {
        rowId = 0;
        ftpURL = new String();
        detailsBookId = 0;
        statusBook = 0;
        book = new java.util.ArrayList();
        try {
            InventoryInfo[] details;
            rowId = booksModel.getRowIndex();
            InventoryInfo item = (InventoryInfo) books.get(rowId);
            detailsBookId = item.getRECNO_2();
            ftpURL = item.getELECTR_RES();
            statusBook = item.getROOTID();
            details = cartDetails.getDescription(detailsBookId);
            if (details != null) {
                book.addAll(Arrays.asList(details));
            }
        } catch (Exception ex) {
            addMessage("Ошибка при поиске : " + ex.getMessage());
            return "error";
        }
        setNewBooks(true);
        return "details";
    }

    public String getShowAttachment() {
        if ((ftpURL != null) && (ftpURL.trim().length() > 0)) {
            return "true";
        }
        return "false";
    }

    /**
     * Listener the value of search text
     *
     * @param valueChangeEvent new value of search text
     */
    public void valueChangedSearchText(ValueChangeEvent valueChangeEvent) {
        searchText = (String) valueChangeEvent.getNewValue();
    }

    /**
     * Listener the value of date from
     *
     * @param valueChangeEvent new value of date from
     */
    public void valueChangedDateFrom(ValueChangeEvent valueChangeEvent) {
        dateFrom = (String) valueChangeEvent.getNewValue();
    }

    /**
     * Listener the value of date to
     *
     * @param valueChangeEvent new value of date to
     */
    public void valueChangedDateTo(ValueChangeEvent valueChangeEvent) {
        dateTo = (String) valueChangeEvent.getNewValue();
    }

    /**
     * URL for selected image
     */
    protected String imageURL;

    /**
     * Get the value of imageURL
     *
     * @return the value of imageURL
     */
    public String getImageURL() {
        return imageURL;
    }

    /**
     * Set the value of imageURL
     *
     * @param imageURL new value of imageURL
     */
    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }
    /**
     * Title for selected image
     */
    protected String title;

    /**
     * Get the value of title
     *
     * @return the value of title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Set the value of title
     *
     * @param title new value of title
     */
    public void setTitle(String title) {
        this.title = title;
    }
    /**
     * Selected search type in a List Box
     */
    protected String searchType;

    /**
     * Get the value of searchType
     *
     * @return the value of searchType
     */
    public String getSearchType() {
        return searchType;
    }

    /**
     * Set the value of searchType
     *
     * @param searchType new value of searchType
     */
    public void setSearchType(String searchType) {
        this.searchType = searchType;
    }
    /**
     * Selected resource in a One Menu
     */
    protected String resource;

    /**
     * Get the value of resource
     *
     * @return the value of resource
     */
    public String getResource() {
        return resource;
    }

    /**
     * Set the value of resource
     *
     * @param resource new value of resource
     */
    public void setResource(String resource) {
        this.resource = resource;
    }
    /**
     * Selected document in a One Menu
     */
    protected String document;

    /**
     * Get the value of document
     *
     * @return the value of document
     */
    public String getDocument() {
        return document;
    }

    /**
     * Set the value of document
     *
     * @param document new value of document
     */
    public void setDocument(String document) {
        this.document = document;
    }
    /**
     * Selected document form in a One Menu
     */
    protected String docform;

    /**
     * Get the value of docform
     *
     * @return the value of docform
     */
    public String getDocform() {
        return docform;
    }

    /**
     * Set the value of docform
     *
     * @param docform new value of docform
     */
    public void setDocform(String docform) {
        this.docform = docform;
    }
    /**
     * Selected document type in a One Menu
     */
    protected String doctype;

    /**
     * Get the value of doctype
     *
     * @return the value of doctype
     */
    public String getDoctype() {
        return doctype;
    }

    /**
     * Set the value of doctype
     *
     * @param doctype new value of doctype
     */
    public void setDoctype(String doctype) {
        this.doctype = doctype;
    }
    /**
     * Selected categorie in a One Menu
     */
    protected String categories;

    /**
     * Get the value of categories
     *
     * @return the value of categories
     */
    public String getCategories() {
        return categories;
    }

    /**
     * Set the value of categories
     *
     * @param categories new value of categories
     */
    public void setCategories(String categories) {
        this.categories = categories;
    }
    /**
     * Prepared string for filtered resource
     */
    protected String filteredResource;

    /**
     * Get the value of filteredResource
     *
     * @return the value of filteredResource
     */
    public String getFilteredResource() {
        return filteredResource;
    }

    /**
     * Set the value of filteredResource
     *
     * @param filteredResource new value of filteredResource
     */
    public void setFilteredResource(String filteredResource) {
        this.filteredResource = filteredResource;
    }
    /**
     * Prepared string for filtered document
     */
    protected String filteredDocument;

    /**
     * Get the value of filteredDocument
     *
     * @return the value of filteredDocument
     */
    public String getFilteredDocument() {
        return filteredDocument;
    }

    /**
     * Set the value of filteredDocument
     *
     * @param filteredDocument new value of filteredDocument
     */
    public void setFilteredDocument(String filteredDocument) {
        this.filteredDocument = filteredDocument;
    }
    /**
     * Prepared string for filtered document form
     */
    protected String filteredDocform;

    /**
     * Get the value of filteredDocform
     *
     * @return the value of filteredDocform
     */
    public String getFilteredDocform() {
        return filteredDocform;
    }

    /**
     * Set the value of filteredDocform
     *
     * @param filteredDocform new value of filteredDocform
     */
    public void setFilteredDocform(String filteredDocform) {
        this.filteredDocform = filteredDocform;
    }
    /**
     * Prepared string for filtered document type
     */
    protected String filteredDoctype;

    /**
     * Get the value of filteredDoctype
     *
     * @return the value of filteredDoctype
     */
    public String getFilteredDoctype() {
        return filteredDoctype;
    }

    /**
     * Set the value of filteredDoctype
     *
     * @param filteredDoctype new value of filteredDoctype
     */
    public void setFilteredDoctype(String filteredDoctype) {
        this.filteredDoctype = filteredDoctype;
    }
    /**
     * Prepared string for filtered categorie
     */
    protected String filteredCategories;

    /**
     * Get the value of filteredCategories
     *
     * @return the value of filteredCategories
     */
    public String getFilteredCategories() {
        return filteredCategories;
    }

    /**
     * Set the value of filteredCategories
     *
     * @param filteredCategories new value of filteredCategories
     */
    public void setFilteredCategories(String filteredCategories) {
        this.filteredCategories = filteredCategories;
    }
    /**
     * Selected number to view records on a page
     */
    protected String viewRecords;

    /**
     * Get the value of viewRecords
     *
     * @return the value of viewRecords
     */
    public String getViewRecords() {
        return viewRecords;
    }

    /**
     * Set the value of viewRecords
     *
     * @param viewRecords new value of viewRecords
     */
    public void setViewRecords(String viewRecords) {
        this.viewRecords = viewRecords;
    }
    /**
     * Value for page navigation
     */
    protected int first;

    /**
     * Get the value of first
     *
     * @return the value of first
     */
    public int getFirst() {
        return first;
    }

    /**
     * Set the value of first
     *
     * @param first new value of first
     */
    public void setFirst(int first) {
        this.first = first;
    }
    /**
     * Value for page navigation
     */
    protected int rows;

    /**
     * Get the value of rows
     *
     * @return the value of rows
     */
    public int getRows() {
        return rows;
    }

    /**
     * Set the value of rows
     *
     * @param rows new value of rows
     */
    public void setRows(int rows) {
        this.rows = rows;
    }

    /**
     * Listener the value of search type
     *
     * @param valueChangeEvent new value of search type
     */
    public void valueChangedSearchType(ValueChangeEvent valueChangeEvent) {
        searchType = (String) valueChangeEvent.getNewValue();
    }

    /**
     * Listener the value of resource
     *
     * @param valueChangeEvent new value of resource
     */
    public void valueChangedResource(ValueChangeEvent valueChangeEvent) {
        resource = (String) valueChangeEvent.getNewValue();
        FacesConfig fc = new FacesConfig();
        fc.setImageResource(resource);
        this.filtrationResource();
    }

    /**
     * Set the value for filtered resource
     */
    public void filtrationResource() {
        if (resource.equals("1")) {
            filteredResource = "%%";
        } else if (resource.equals("2")) {
            filteredResource = "%am0%";
        } else if (resource.equals("3")) {
            filteredResource = "%am1%";
        } else if (resource.equals("4")) {
            filteredResource = "%am2%";
        } else if (resource.equals("5")) {
            filteredResource = "%am3%";
        } else if (resource.equals("6")) {
            filteredResource = "%as1%";
        } else if (resource.equals("7")) {
            filteredResource = "%as2%";
        } else if (resource.equals("8")) {
            filteredResource = "%as3%";
        } else if (resource.equals("9")) {
            filteredResource = "%aa%";
        } else if (resource.equals("10")) {
            filteredResource = "%lm0%";
        }
    }

    /**
     * Listener the value of document
     *
     * @param valueChangeEvent new value of document
     */
    public void valueChangedDocument(ValueChangeEvent valueChangeEvent) {
        document = (String) valueChangeEvent.getNewValue();
        this.filtrationDocument();
    }

    /**
     * Set the value for filtered document
     */
    public void filtrationDocument() {
        if (document.equals("1")) {
            filteredDocument = "%%";
        } else if (document.equals("2")) {
            filteredDocument = "%__m%";
        } else if (document.equals("3")) {
            filteredDocument = "%__s%";
        } else if (document.equals("4")) {
            filteredDocument = "%__a%";
        }
    }

    /**
     * Listener the value of document form
     *
     * @param valueChangeEvent new value of document form
     */
    public void valueChangedDocForm(ValueChangeEvent valueChangeEvent) {
        docform = (String) valueChangeEvent.getNewValue();
        this.filtrationDocForm();
    }

    /**
     * Set the value for filtered document form
     */
    public void filtrationDocForm() {
        if (docform.equals("1")) {
            filteredDocform = "%%";
        } else if (docform.equals("2")) {
            filteredDocform = "%_____a%";
        } else if (docform.equals("3")) {
            filteredDocform = "%_____b%";
        } else if (docform.equals("4")) {
            filteredDocform = "%_____c%";
        } else if (docform.equals("5")) {
            filteredDocform = "%_____r%";
        } else if (docform.equals("6")) {
            filteredDocform = "%_____d%";
        } else if (docform.equals("7")) {
            filteredDocform = "%_____h%";
        } else if (docform.equals("8")) {
            filteredDocform = "%_____n%";
        }
    }

    /**
     * Listener the value of document type
     *
     * @param valueChangeEvent new value of document type
     */
    public void valueChangedDocType(ValueChangeEvent valueChangeEvent) {
        doctype = (String) valueChangeEvent.getNewValue();
        this.filtrationDocType();
    }

    /**
     * Set the value for filtered document type
     */
    public void filtrationDocType() {
        if (doctype.equals("1")) {
            filteredDoctype = "%%";
        } else if (doctype.equals("2")) {
            filteredDoctype = "%____b%";
        } else if (doctype.equals("3")) {
            filteredDoctype = "%____e%";
        } else if (doctype.equals("4")) {
            filteredDoctype = "%____f%";
        } else if (doctype.equals("5")) {
            filteredDoctype = "%____h%";
        } else if (doctype.equals("6")) {
            filteredDoctype = "%____m%";
        }
    }

    /**
     * Listener the value of categories
     *
     * @param valueChangeEvent new value of categories
     */
    public void valueChangedCategories(ValueChangeEvent valueChangeEvent) {
        categories = (String) valueChangeEvent.getNewValue();
        this.filtrationCategories();
    }

    /**
     * Set the value for filtered categories
     */
    public void filtrationCategories() {
        if (categories.equals("1")) {
            filteredCategories = "%%";
        } else if (categories.equals("2")) {
            filteredCategories = "%______a%";
        } else if (categories.equals("3")) {
            filteredCategories = "%______b%";
        } else if (categories.equals("4")) {
            filteredCategories = "%______c%";
        } else if (categories.equals("5")) {
            filteredCategories = "%______f%";
        } else if (categories.equals("6")) {
            filteredCategories = "%______k%";
        } else if (categories.equals("7")) {
            filteredCategories = "%______t%";
        } else if (categories.equals("8")) {
            filteredCategories = "%______v%";
        } else if (categories.equals("9")) {
            filteredCategories = "%______u%";
        } else if (categories.equals("10")) {
            filteredCategories = "%______p%";
        }
    }

    /**
     * Listener the value of quantity records on a page
     *
     * @param valueChangeEvent new value of number records on a page
     */
    public void valueChangedRecords(ValueChangeEvent valueChangeEvent) {
        viewRecords = (String) valueChangeEvent.getNewValue();
        this.changeRecords();
    }

    /**
     * Set the value of quantity records on a page
     */
    public void changeRecords() {
        if (viewRecords.equals("1")) {
            this.setRows(10);
        } else if (viewRecords.equals("2")) {
            this.setRows(20);
        } else if (viewRecords.equals("3")) {
            this.setRows(30);
        }
    }

    /**
     * Navigation on preview page
     */
    public void doActionPrev() {
        first = first - rows;
        this.setFirst(first);
        this.openSearchResults();
    }

    /**
     * Navigation on next page
     */
    public void doActionNext() {
        first = first + rows;
        this.setFirst(first);
        this.openSearchResults();
    }

    /**
     * Navigation on preview page
     */
    public void doActionPrevBooks() {
        first = first - 10;
        this.setFirst(first);
        this.openNewBooksResults();
    }

    /**
     * Navigation on next page
     */
    public void doActionNextBooks() {
        first = first + 10;
        this.setFirst(first);
        this.openNewBooksResults();
    }

    /**
     * Get the value of newBooks
     *
     * @return the value of newBooks
     */
    public boolean isNewBooks() {
        return newBooks;
    }

    /**
     * Set the value of newBooks
     *
     * @param newBooks new value of newBooks
     */
    public void setNewBooks(boolean newBooks) {
        this.newBooks = newBooks;
    }
}
