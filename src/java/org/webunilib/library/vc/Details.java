package org.webunilib.library.vc;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import org.webunilib.library.m.data.InventoryInfo;
import org.webunilib.library.m.db.DbAccess;

/**
 * Copyright © 2009 Jet Thoughts, Ltd. All rights reserved.
 *
 * @author Kirillov A. A.
 */
@ManagedBean
@RequestScoped
public class Details implements Serializable {

    protected DbAccess cartDetails;
    protected List book;
    protected Integer detailsBookId;
    protected Integer statusBook;
    protected boolean newBooks;

    /**
     * Creates a new instance of Details
     */
    public Details() {
        init();
    }

    public String getId() {
        FacesContext context = FacesContext.getCurrentInstance();
        Map map = context.getExternalContext().getRequestParameterMap();
        return (String) map.get("id");
    }

    private void init() {
        cartDetails = new DbAccess();
        detailsBookId = Integer.parseInt(this.getId());
        statusBook = 0;
        book = new java.util.ArrayList();
        try {
            InventoryInfo[] details;
            details = cartDetails.getDescription(detailsBookId);
            if (details != null) {
                book.addAll(Arrays.asList(details));
            }
            InventoryInfo selectedId = (InventoryInfo) book.get(0);
            statusBook = selectedId.getROOTID();
        } catch (Exception ex) {
            addMessage("Ошибка при поиске : " + ex.getMessage());
        }
        newBooks = true;
    }

    private void addMessage(String message) {
        FacesContext context = FacesContext.getCurrentInstance();
        FacesMessage msg = new FacesMessage(message);
        context.addMessage(null, msg);
    }

    public List getBook() {
        return book;
    }

    public Integer getStatusBook() {
        return statusBook;
    }

    public Boolean getNewBooks() {
        return newBooks;
    }
}
